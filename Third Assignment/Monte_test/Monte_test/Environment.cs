﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class Environment
    {
        static public string optionType;

        static public double spotPrice;
        static public double strikePrice;
        static public int step;
        static public double risklessInterest;
        static public double volatility;
        static public int trials;
        static public double pc_idx;
        static public int anti_idx;
        static public int cv_idx;
        static public int trading_frequency;

        static public double unitInterval;
        static public double tenor;
        static public double discount;
        static public int split = 1000;

        static public double increS = 0.01;
        static public double increR = 0.0001;
        static public double increV = 0.0001;

        static public List<Queue<double>> randlist_collection;
        static public Queue<double> temp;
        static public List<Queue<double>> price;
        static public List<Queue<double>> delta;
        static public List<Queue<double>> theta;
        static public List<Queue<double>> rho;
        static public List<Queue<double>> vega;
        static public List<Queue<double>> gamma;
        static public List<Queue<double>> cvList;
        static public List<Queue<double>> SN_next_List;
        static public List<Queue<double>> SN_List;


        static public void init_environment()
        {
            step = Convert.ToInt32(Math.Ceiling(tenor * trading_frequency));
            discount = Math.Exp(-Environment.tenor * Environment.risklessInterest);
            unitInterval = 1.0 / trading_frequency;

            temp = new Queue<double>();
            price = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            delta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            theta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            rho = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            vega = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            gamma = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            cvList = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            SN_next_List = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            SN_List = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
        }
        static public void fillQueue()
        {
            normalrandom randlist = new normalrandom(Convert.ToInt32(Math.Ceiling(1.0 * trials * (step + 1) / split)));
            randlist_collection = new List<Queue<double>>(2) { randlist.randomlist_posi, randlist.randomlist_nega };
        }
        static public double se(Queue<double> queue)
        {
            double se = 0;
            double mean = queue.Average();
            double length = queue.Count();
            while (queue.Count() > 0)
            {
                double temp = queue.Dequeue();
                se = se + (temp - mean) * (temp - mean);
            }
            return (Math.Sqrt(se / (length * (length - 1))) * discount);
        }        
    }
}
