﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class Environment
    {
        static public double spotPrice;
        static public double strikePrice;
        static public int step;
        static public double risklessInterest;
        static public double volatility;
        static public int trials;
        static public double pc_idx;
        static public int trading_frequency;

        static public double unitInterval;
        static public double tenor;
        static public double discount;
        static public int split = 1000;

        static public double increS = 0.01;
        static public double increR = 0.0001;
        static public double increV = 0.001;

        static public Queue<double> randlist;
        static public Queue<double> temp = new Queue<double>();
        static public Queue<double> price = new Queue<double>();


        static public void init_environment()
        {
            step = Convert.ToInt32(Math.Ceiling(tenor * trading_frequency));
            discount = Math.Exp(-Environment.tenor * Environment.risklessInterest);
            unitInterval = 1.0 / trading_frequency;
        }
        static public void fillQueue()
        {
            randlist = new normalrandom(Convert.ToInt32(Math.Ceiling(1.0 * trials * step / split))).randomlist;
        }
        static public double se(Queue<double> queue)
        {
            double mean = queue.Average();
            double se = 0;
            double length = queue.Count();
            while(queue.Count()>0)
            {
                double temp = queue.Dequeue();
                se = se + (temp - mean) * (temp - mean);
            }
            return (Math.Sqrt(se / (length * (length - 1))) * discount);
        }
    }
}
