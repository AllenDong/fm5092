﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class normalrandom
    {

        public normalrandom(int amt)
        {
            Porlar(amt);
        }

        private void Porlar(int amt)
        {
            Queue<double> queue = new Queue<double>();
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            double randn1, randn2;
            double[] z = new double[2];
            while (queue.Count() < amt)
            {
                double w = 2;
                do
                {
                    randn1 = rnd.NextDouble() * 2 - 1.0;
                    randn2 = rnd.NextDouble() * 2 - 1.0;
                    w = randn1 * randn1 + randn2 * randn2;

                } while (w > 1);
                z[0] = Math.Sqrt(-2 * Math.Log(w) / w);
                z[1] = z[0] * randn1;
                z[0] = z[0] * randn2;
                queue.Enqueue(z[1]);
                queue.Enqueue(z[0]);
            }
            this.randomlist = queue;
        }
        public Queue<double> randomlist = null;
    }
}
