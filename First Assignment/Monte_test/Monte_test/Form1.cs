﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            WarnTxt.Hide();
            groupBox1.Hide();
            SimulatorStart.Hide();
        }

        static public ResultForm resul = new ResultForm();

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                resul.progressBar.Value = 0;
                Environment.spotPrice = Convert.ToDouble(SpotPriceInput.Text);
                Environment.strikePrice = Convert.ToDouble(StrikePriceInput.Text);
                Environment.risklessInterest = Convert.ToDouble(InterestRateInput.Text);
                Environment.volatility = Convert.ToDouble(VolatilityInput.Text);
                Environment.tenor = Convert.ToDouble(TenorInput.Text);
                Environment.trials = Convert.ToInt32(TrailInput.Text);
                Environment.trading_frequency = Convert.ToInt32(frequencybox.Text);

                resul.progressBar.Maximum = Environment.split;
                if (CallButton.Checked)
                {
                    Environment.pc_idx = 1;
                }
                else
                {
                    Environment.pc_idx = -1;
                }
                WarnTxt.Hide();
                resul.ShowDialog();
            }
            catch
            {
                WarnTxt.Show();
            }

        }

        private void europeanOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void americanOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Show();
            SimulatorStart.Hide();
        }

        private void asianOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Show();
            SimulatorStart.Hide();
        }

        private void barrierOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Show();
            SimulatorStart.Hide();
        }

        private void lookBackOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Show();
            SimulatorStart.Hide();
        }

        private void digitalOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox3.Show();
            SimulatorStart.Hide();
        }
        
    }
}
