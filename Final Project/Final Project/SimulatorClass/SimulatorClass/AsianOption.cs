﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SimulatorClass
{
    class AsianOption : IOptions
    {
        public AsianOption(ConcurrentQueue<double> randlist, int pn_idx)
        {
            this.pn_idx = pn_idx;
            pathGene(randlist);
        }
        public void pathGene(ConcurrentQueue<double> randlist)
        {
            double delta;
            double advRate = Math.Exp(Environment.risklessInterest * Environment.unitInterval);
            double lastPrice = Environment.spotPrice;
            double lastPrice_rho = Environment.spotPrice;
            double lastPrice_vega = Environment.spotPrice;
            double temp;
            double temp_dequeue = 0;
            for (int idx = 0; idx < Environment.step + 1; idx++)
            {
                temp = lastPrice;
                path.Enqueue(lastPrice);
                path_rho.Enqueue(lastPrice_rho);
                path_vega.Enqueue(lastPrice_vega);
                randlist.TryDequeue(out temp_dequeue);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                lastPrice_rho = lastPrice_rho * Math.Exp(((Environment.risklessInterest + Environment.increR - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                lastPrice_vega = lastPrice_vega * Math.Exp(((Environment.risklessInterest - (Environment.volatility + Environment.increV) * (Environment.volatility + Environment.increV) / 2) * Environment.unitInterval) + (Environment.volatility + Environment.increV) * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                if (Environment.cv_idx == 1)
                {
                    delta = DeltaCV.deltaGene(Environment.pc_idx == 1 ? true : false, temp, Environment.strikePrice, Environment.risklessInterest, Environment.volatility, Environment.tenor, idx * Environment.unitInterval);
                    if (idx != Environment.step)
                    {
                        contro_var = contro_var + delta * (lastPrice - temp * advRate);
                    }
                }
            }
            Sn = path.Average();
            Sn_rho = path_rho.Average();
            Sn_vega = path_vega.Average();
            path.Enqueue(lastPrice);
            Sn_next = path.Average();
        }
        public static void greekLetter(ConcurrentQueue<IOptions> asqueue)
        {
            IOptions asianOption;
            while (asqueue.Count() > 0)
            {
                asqueue.TryDequeue(out asianOption);
                double SN = asianOption.Sn;
                double SN_next = asianOption.Sn_next;
                double finalValue = Math.Max(0, Environment.pc_idx * (SN - Environment.strikePrice));
                double finalValue_theta = Math.Max(0, Environment.pc_idx * (SN_next - Environment.strikePrice));
                double finalValue_delta = Math.Max(0, Environment.pc_idx * (SN * (1 + Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                double finalValue_delta2 = Math.Max(0, Environment.pc_idx * (SN * (1 - Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                double finalValue_gamma = (finalValue_delta - 2 * finalValue + finalValue_delta2) / (Environment.increS * Environment.increS);
                double finalValue_rho = Math.Max(0, Environment.pc_idx * (asianOption.Sn_rho - Environment.strikePrice));
                double finalValue_vega = Math.Max(0, Environment.pc_idx * (asianOption.Sn_vega - Environment.strikePrice));
                int idx = asianOption.pn_idx;
                Environment.price[idx].Enqueue(finalValue);
                Environment.theta[idx].Enqueue(finalValue_theta);
                Environment.delta[idx].Enqueue(finalValue_delta);
                Environment.rho[idx].Enqueue(finalValue_rho);
                Environment.vega[idx].Enqueue(finalValue_vega);
                Environment.gamma[idx].Enqueue(finalValue_gamma);
                Environment.cvList[idx].Enqueue(asianOption.contro_var);
            }
        }
    }
}
