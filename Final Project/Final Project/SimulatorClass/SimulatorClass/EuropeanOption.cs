﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SimulatorClass
{
    class EuropeanOption : IOptions
    {
        public EuropeanOption(ConcurrentQueue<double> randlist,int pn_idx)
        {
            this.pn_idx = pn_idx;
            pathGene(randlist);
        }
        public void pathGene(ConcurrentQueue<double> randlist)
        {
            double delta;
            double advRate = Math.Exp(Environment.risklessInterest * Environment.unitInterval);
            double lastPrice = Environment.spotPrice;
            double temp;
            double temp_dequeue = 0;
            for (int idx = 0; idx < Environment.step + 1; idx++)
            {
                temp = lastPrice;
                path.Enqueue(lastPrice);
                randlist.TryDequeue(out temp_dequeue);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                if (Environment.cv_idx == 1)
                {
                    delta = DeltaCV.deltaGene(Environment.pc_idx==1? true : false, temp, Environment.strikePrice, Environment.risklessInterest, Environment.volatility, Environment.tenor, idx * Environment.unitInterval);
                    if (idx != Environment.step)
                    {
                        contro_var = contro_var + delta * (lastPrice - temp * advRate);
                    }
                }
            }
            path.Enqueue(lastPrice);
            Sn = path.ElementAt(path.Count() - 2);
            Sn_next = path.ElementAt(path.Count() - 1);
        }
        public static void greekLetter(ConcurrentQueue<IOptions> eu_queue)
        {
            IOptions europeanOption;
            while (eu_queue.Count() > 0)
            {
                eu_queue.TryDequeue(out europeanOption);
                double SN = europeanOption.Sn;
                double SN_next = europeanOption.Sn_next;
                double finalValue = Math.Max(0, Environment.pc_idx * (SN - Environment.strikePrice));
                double finalValue_delta = Math.Max(0, Environment.pc_idx * (SN * (1 + Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                double finalValue_delta2 = Math.Max(0, Environment.pc_idx * (SN * (1 - Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                double finalValue_gamma = (finalValue_delta - 2 * finalValue + finalValue_delta2) / (Environment.increS * Environment.increS);
                double finalValue_theta = Math.Max(0, Environment.pc_idx * (SN_next - Environment.strikePrice));
                double finalValue_rho = Math.Max(0, Environment.pc_idx * (Math.Exp(Environment.increR * Environment.tenor) * SN - Environment.strikePrice));
                double finalValue_vega = Math.Max(0, Environment.pc_idx * (SN * Math.Exp(-(Environment.increV * Environment.increV / 2 + Environment.increV * Environment.volatility) * Environment.tenor + (Math.Log(SN / Environment.spotPrice) - (Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.tenor) / Environment.volatility * Environment.increV) - Environment.strikePrice));
                int idx = europeanOption.pn_idx;
                Environment.price[idx].Enqueue(finalValue);
                Environment.theta[idx].Enqueue(finalValue_theta);
                Environment.delta[idx].Enqueue(finalValue_delta);
                Environment.rho[idx].Enqueue(finalValue_rho);
                Environment.vega[idx].Enqueue(finalValue_vega);
                Environment.gamma[idx].Enqueue(finalValue_gamma);
                Environment.cvList[idx].Enqueue(europeanOption.contro_var);
            }
        }
    }
}
