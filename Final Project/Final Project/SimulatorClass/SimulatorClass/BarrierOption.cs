﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SimulatorClass
{
    class BarrierOption : IOptions
    {
        public BarrierOption(ConcurrentQueue<double> randlist, int pn_idx)
        {
            this.pn_idx = pn_idx;
            pathGene(randlist);
        }
        public void pathGene(ConcurrentQueue<double> randlist)
        {
            double delta;
            double advRate = Math.Exp(Environment.risklessInterest * Environment.unitInterval);
            double lastPrice = Environment.spotPrice;
            double lastPrice_rho = Environment.spotPrice;
            double lastPrice_vega = Environment.spotPrice;
            double temp;
            double temp_dequeue = 0;
            for (int idx = 0; idx < Environment.step + 1; idx++)
            {
                temp = lastPrice;
                path.Enqueue(lastPrice);
                path_rho.Enqueue(lastPrice_rho);
                path_vega.Enqueue(lastPrice_vega);
                randlist.TryDequeue(out temp_dequeue);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                lastPrice_rho = lastPrice_rho * Math.Exp(((Environment.risklessInterest + Environment.increR - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                lastPrice_vega = lastPrice_vega * Math.Exp(((Environment.risklessInterest - (Environment.volatility + Environment.increV) * (Environment.volatility + Environment.increV) / 2) * Environment.unitInterval) + (Environment.volatility + Environment.increV) * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                if (Environment.cv_idx == 1)
                {
                    delta = DeltaCV.deltaGene(Environment.pc_idx == 1 ? true : false, temp, Environment.strikePrice, Environment.risklessInterest, Environment.volatility, Environment.tenor, idx * Environment.unitInterval);
                    if (idx != Environment.step)
                    {
                        contro_var = contro_var + delta * (lastPrice - temp * advRate);
                    }
                }
            }
            if (Environment.up_down_idx == 1) //up
            {
                Sn_knock = path.Max();
                Sn_rho_knock = path_rho.Max();
                Sn_vega_knock = path_vega.Max();
                Sn_next_knock = Math.Max(lastPrice, path.Max());
            }
            else
            {
                Sn_knock = path.Min();
                Sn_rho_knock = path_rho.Min();
                Sn_vega_knock = path_vega.Min();
                Sn_next_knock = Math.Min(lastPrice, path.Min());
            }
            Sn = path.Last();
            Sn_rho = path_rho.Last();
            Sn_vega = path_vega.Last();
            path.Enqueue(lastPrice);
            Sn_next = path.Last();
        }
        public static void greekLetter(ConcurrentQueue<IOptions> br_queue)
        {
            IOptions barrierOption;
            int knock = Environment.out_in_idx* Environment.up_down_idx;
            while (br_queue.Count() > 0)
            {
                br_queue.TryDequeue(out barrierOption);
                double finalValue = knock * (barrierOption.Sn_knock - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn - Environment.strikePrice)) : 0;
                double finalValue_theta = knock * (barrierOption.Sn_next_knock - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn_next - Environment.strikePrice)) : 0;
                double finalValue_delta = knock * (barrierOption.Sn_knock * (1 + Environment.increS / Environment.spotPrice) - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn * (1 + Environment.increS / Environment.spotPrice) - Environment.strikePrice)) : 0;
                double finalValue_delta2 = knock * (barrierOption.Sn_knock * (1 - Environment.increS / Environment.spotPrice) - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn * (1 - Environment.increS / Environment.spotPrice) - Environment.strikePrice)) : 0;
                double finalValue_gamma = (finalValue_delta - 2 * finalValue + finalValue_delta2) / (Environment.increS * Environment.increS);
                double finalValue_rho = knock * (barrierOption.Sn_rho_knock - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn_rho - Environment.strikePrice)) : 0;
                double finalValue_vega = knock * (barrierOption.Sn_vega_knock - Environment.barrier) > 0 ? Math.Max(0, Environment.pc_idx * (barrierOption.Sn_vega - Environment.strikePrice)) : 0;
                int idx = barrierOption.pn_idx;
                Environment.price[idx].Enqueue(finalValue);
                Environment.theta[idx].Enqueue(finalValue_theta);
                Environment.delta[idx].Enqueue(finalValue_delta);
                Environment.rho[idx].Enqueue(finalValue_rho);
                Environment.vega[idx].Enqueue(finalValue_vega);
                Environment.gamma[idx].Enqueue(finalValue_gamma);
                Environment.cvList[idx].Enqueue(barrierOption.contro_var);
            }
        }
    }
}
