﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace SimulatorClass
{
        public enum Option_Type { Stock, European, Range, Asian, Digital, Lookback, Barrier };
        public enum Cal_Type { Call, Put, Down_Out, Down_In, Up_Out, Up_In }
        public enum Method_Type { Antithetic, Multi_Thread, Control_Variance }
    public class Monte_Simulator
    {
        static public void Monte_Simulate(Option_Type option_Type, double spot_Price, double strike_Price, double interest_Rate, double volatility, double tenor, int trails, int frequency, Cal_Type[] cal_Type, Method_Type[] method_Type, params object[] args)
        {
            Environment.spotPrice = spot_Price;
            Environment.risklessInterest = interest_Rate;
            Environment.volatility = volatility;
            Environment.tenor = tenor;
            Environment.trials = trails;
            Environment.trading_frequency = frequency;
            Environment.thread_amt = method_Type.Contains(Method_Type.Multi_Thread) ? System.Environment.ProcessorCount : 1;
            Environment.anti_idx = method_Type.Contains(Method_Type.Antithetic) ? 1 : 0;
            Environment.cv_idx = method_Type.Contains(Method_Type.Control_Variance) ? 1 : 0;
            Environment.split = Environment.thread_amt;
            Environment.pc_idx = cal_Type.Contains(Cal_Type.Call) ? 1 : -1;
            switch (option_Type)
            {
                case Option_Type.European:
                    {
                        Environment.optionType = typeof(EuropeanOption);
                        Environment.strikePrice = strike_Price;

                        StartCal_Click();
                        break;
                    }
                case Option_Type.Range:
                    {
                        Environment.optionType = typeof(RangeOption);

                        StartCal_Click();
                        break;
                    }
                case Option_Type.Asian:
                    {
                        Environment.optionType = typeof(AsianOption);
                        Environment.strikePrice = strike_Price;

                        StartCal_Click();
                        break;
                    }
                case Option_Type.Digital:
                    {
                        Environment.optionType = typeof(DigitalOption);
                        Environment.strikePrice = strike_Price;
                        foreach (object obj in args) { Environment.rebate = (double)obj; }

                        StartCal_Click();
                        break;
                    }
                case Option_Type.Lookback:
                    {
                        Environment.optionType = typeof(LookbackOption);
                        Environment.strikePrice = strike_Price;

                        StartCal_Click();
                        break;
                    }
                case Option_Type.Barrier:
                    {
                        Environment.optionType = typeof(BarrierOption);
                        Environment.strikePrice = strike_Price;
                        foreach (object obj in args) { Environment.barrier = (double)obj; }
                        Environment.out_in_idx = (cal_Type.Contains(Cal_Type.Up_In) || cal_Type.Contains(Cal_Type.Down_In)) ? 1 : -1;
                        Environment.up_down_idx = (cal_Type.Contains(Cal_Type.Up_In) || cal_Type.Contains(Cal_Type.Up_Out)) ? 1 : -1;

                        StartCal_Click();
                        break;
                    }
                default:
                    {
                        Environment.out_price = spot_Price;
                        Environment.out_delta = 0;
                        Environment.out_theta = 0;
                        Environment.out_gamma = 0;
                        Environment.out_rho = 0;
                        Environment.out_vega = 0;
                        break;
                    }
            }
        }


        static Thread simulator_thread;
        static private void StartCal_Click()
        {
            //Form1.resul.progressBar.Value = 0;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            Environment.init_environment();
            try
            {
                simulator_thread = new Thread(new ThreadStart(Environment.simulate));//EuropeanOption.simulate));
                simulator_thread.Start();
                simulator_thread.Join();

            }
            catch
            {
                simulator_thread.Abort();
                simulator_thread = new Thread(new ThreadStart(Environment.simulate));//EuropeanOption.simulate));
                simulator_thread.Start();
                simulator_thread.Join();
            }
            //StartCal.Text = "Price : " + Convert.ToString(Environment.out_price);
            //thetabox.Text = Convert.ToString(Environment.out_theta);
            //deltabox.Text = Convert.ToString(Environment.out_delta);
            //gammabox.Text = Convert.ToString(Environment.out_gamma);
            //rhobox.Text = Convert.ToString(Environment.out_rho);
            //vegabox.Text = Convert.ToString(Environment.out_vega);
            //stderror.Text = Convert.ToString(Environment.se(Environment.price[0]));
            watch.Stop();
            TimeSpan ts = new TimeSpan();
            ts = watch.Elapsed;
            //TimeLen.Text = Convert.ToString(ts);
        }

    }
}
