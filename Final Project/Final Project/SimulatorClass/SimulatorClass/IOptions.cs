﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulatorClass
{
    abstract class IOptions
    {
        void pathGene(ConcurrentQueue<double> randlist) { }
        public Queue<double> path = new Queue<double>();
        public Queue<double> path_rho = new Queue<double>();
        public Queue<double> path_vega = new Queue<double>();
        public double Sn;
        public double Sn_next;
        public double Sn_rho;
        public double Sn_vega;
        public double Sn_knock;
        public double Sn_rho_knock;
        public double Sn_vega_knock;
        public double Sn_next_knock;
        public int pn_idx;
        public double contro_var;
    }
}
