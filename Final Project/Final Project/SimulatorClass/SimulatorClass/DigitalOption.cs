﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SimulatorClass
{
    class DigitalOption : IOptions
    {
        public DigitalOption(ConcurrentQueue<double> randlist, int pn_idx)
        {
            this.pn_idx = pn_idx;
            pathGene(randlist);
        }
        public void pathGene(ConcurrentQueue<double> randlist)
        {
            double delta;
            double advRate = Math.Exp(Environment.risklessInterest * Environment.unitInterval);
            double lastPrice = Environment.spotPrice;
            double temp;
            double temp_dequeue = 0;
            for (int idx = 0; idx < Environment.step + 1; idx++)
            {
                temp = lastPrice;
                path.Enqueue(lastPrice);
                randlist.TryDequeue(out temp_dequeue);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * temp_dequeue);
                if (Environment.cv_idx == 1)
                {
                    delta = DeltaCV.deltaGene(Environment.pc_idx == 1 ? true : false, temp, Environment.strikePrice, Environment.risklessInterest, Environment.volatility, Environment.tenor, idx * Environment.unitInterval);
                    if (idx != Environment.step)
                    {
                        contro_var = contro_var + delta * (lastPrice - temp * advRate);
                    }
                }
            }
            Sn = path.Last();//Average();
            path.Enqueue(lastPrice);
            Sn_next = path.Last();// Average();
        }
        public static void greekLetter(ConcurrentQueue<IOptions> dg_queue)
        {
            IOptions digitalOption;
            while (dg_queue.Count() > 0)
            {
                dg_queue.TryDequeue(out digitalOption);
                double SN = digitalOption.Sn;
                double SN_next = digitalOption.Sn_next;
                double finalValue = (Environment.pc_idx * SN) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                double finalValue_theta = (Environment.pc_idx * SN_next) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                double finalValue_delta = (Environment.pc_idx * SN * (1 + Environment.increS / Environment.spotPrice)) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                double finalValue_delta2 = (Environment.pc_idx * SN * (1 - Environment.increS / Environment.spotPrice)) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                double finalValue_gamma = (finalValue_delta - 2 * finalValue + finalValue_delta2) / (Environment.increS * Environment.increS);
                double finalValue_rho = (Environment.pc_idx * (Math.Exp(Environment.increR * Environment.tenor) * SN)) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                double finalValue_vega = (Environment.pc_idx * (SN * Math.Exp(-(Environment.increV * Environment.increV / 2 + Environment.increV * Environment.volatility) * Environment.tenor + (Math.Log(SN / Environment.spotPrice) - (Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.tenor) / Environment.volatility * Environment.increV))) > (Environment.pc_idx * Environment.strikePrice) ? Environment.rebate : 0;
                int idx = digitalOption.pn_idx;
                Environment.price[idx].Enqueue(finalValue);
                Environment.theta[idx].Enqueue(finalValue_theta);
                Environment.delta[idx].Enqueue(finalValue_delta);
                Environment.rho[idx].Enqueue(finalValue_rho);
                Environment.vega[idx].Enqueue(finalValue_vega);
                Environment.gamma[idx].Enqueue(finalValue_gamma);
                Environment.cvList[idx].Enqueue(digitalOption.contro_var);
            }
        }
    }
}
