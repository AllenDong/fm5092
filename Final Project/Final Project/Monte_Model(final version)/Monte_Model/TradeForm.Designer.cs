﻿namespace Monte_Model
{
    partial class TradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.comboBoxBS = new System.Windows.Forms.ComboBox();
            this.comboBoxInstrument = new System.Windows.Forms.ComboBox();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.labelInstrument = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelTimeStamp = new System.Windows.Forms.Label();
            this.labelBS = new System.Windows.Forms.Label();
            this.buttonAddTrade = new System.Windows.Forms.Button();
            this.dateTimePickerTimeStamp = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrice.Location = new System.Drawing.Point(270, 129);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(186, 30);
            this.textBoxPrice.TabIndex = 28;
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuantity.Location = new System.Drawing.Point(270, 84);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(186, 30);
            this.textBoxQuantity.TabIndex = 27;
            // 
            // comboBoxBS
            // 
            this.comboBoxBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxBS.FormattingEnabled = true;
            this.comboBoxBS.Items.AddRange(new object[] {
            "Buy",
            "Sell"});
            this.comboBoxBS.Location = new System.Drawing.Point(270, 36);
            this.comboBoxBS.Name = "comboBoxBS";
            this.comboBoxBS.Size = new System.Drawing.Size(186, 33);
            this.comboBoxBS.TabIndex = 26;
            // 
            // comboBoxInstrument
            // 
            this.comboBoxInstrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxInstrument.FormattingEnabled = true;
            this.comboBoxInstrument.Location = new System.Drawing.Point(270, 216);
            this.comboBoxInstrument.Name = "comboBoxInstrument";
            this.comboBoxInstrument.Size = new System.Drawing.Size(186, 33);
            this.comboBoxInstrument.TabIndex = 25;
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuantity.Location = new System.Drawing.Point(42, 89);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(85, 25);
            this.labelQuantity.TabIndex = 24;
            this.labelQuantity.Text = "Quantity";
            // 
            // labelInstrument
            // 
            this.labelInstrument.AutoSize = true;
            this.labelInstrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstrument.Location = new System.Drawing.Point(42, 224);
            this.labelInstrument.Name = "labelInstrument";
            this.labelInstrument.Size = new System.Drawing.Size(103, 25);
            this.labelInstrument.TabIndex = 23;
            this.labelInstrument.Text = "Instrument";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrice.Location = new System.Drawing.Point(42, 134);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(56, 25);
            this.labelPrice.TabIndex = 22;
            this.labelPrice.Text = "Price";
            // 
            // labelTimeStamp
            // 
            this.labelTimeStamp.AutoSize = true;
            this.labelTimeStamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeStamp.Location = new System.Drawing.Point(42, 181);
            this.labelTimeStamp.Name = "labelTimeStamp";
            this.labelTimeStamp.Size = new System.Drawing.Size(118, 25);
            this.labelTimeStamp.TabIndex = 21;
            this.labelTimeStamp.Text = "Time Stamp";
            // 
            // labelBS
            // 
            this.labelBS.AutoSize = true;
            this.labelBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBS.Location = new System.Drawing.Point(42, 44);
            this.labelBS.Name = "labelBS";
            this.labelBS.Size = new System.Drawing.Size(95, 25);
            this.labelBS.TabIndex = 20;
            this.labelBS.Text = "Buy / Sell";
            // 
            // buttonAddTrade
            // 
            this.buttonAddTrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddTrade.Location = new System.Drawing.Point(47, 275);
            this.buttonAddTrade.Name = "buttonAddTrade";
            this.buttonAddTrade.Size = new System.Drawing.Size(409, 41);
            this.buttonAddTrade.TabIndex = 30;
            this.buttonAddTrade.Text = "ADD THE TRADING";
            this.buttonAddTrade.UseVisualStyleBackColor = true;
            this.buttonAddTrade.Click += new System.EventHandler(this.buttonAddTrade_Click);
            // 
            // dateTimePickerTimeStamp
            // 
            this.dateTimePickerTimeStamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerTimeStamp.Location = new System.Drawing.Point(270, 176);
            this.dateTimePickerTimeStamp.Name = "dateTimePickerTimeStamp";
            this.dateTimePickerTimeStamp.Size = new System.Drawing.Size(186, 30);
            this.dateTimePickerTimeStamp.TabIndex = 31;
            // 
            // TradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 328);
            this.Controls.Add(this.dateTimePickerTimeStamp);
            this.Controls.Add(this.buttonAddTrade);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.comboBoxBS);
            this.Controls.Add(this.comboBoxInstrument);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.labelInstrument);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelTimeStamp);
            this.Controls.Add(this.labelBS);
            this.Name = "TradeForm";
            this.Text = "TradeForm";
            this.Load += new System.EventHandler(this.TradeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.ComboBox comboBoxBS;
        private System.Windows.Forms.ComboBox comboBoxInstrument;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.Label labelInstrument;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelTimeStamp;
        private System.Windows.Forms.Label labelBS;
        private System.Windows.Forms.Button buttonAddTrade;
        private System.Windows.Forms.DateTimePicker dateTimePickerTimeStamp;
    }
}