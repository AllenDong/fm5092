﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class TradeForm : Form
    {
        static public List<object> tradeList;
        public TradeForm()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            InitializeComponent();
            foreach (var instruments in context.InstrumentSet) { comboBoxInstrument.Items.Add(instruments.Ticker); }
        }
        private void TradeForm_Load(object sender, EventArgs e)
        {
        }
        private void buttonAddTrade_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                Boolean bs = (comboBoxBS.Text == "Buy");
                double quantity = Convert.ToDouble(textBoxQuantity.Text);
                double price = Convert.ToDouble(textBoxPrice.Text);
                DateTime timestamp = dateTimePickerTimeStamp.Value;
                Instrument instrument = (from instrumentset in context.InstrumentSet where instrumentset.Ticker == comboBoxInstrument.Text select instrumentset).FirstOrDefault();
                Price markprice = (from prices in context.PriceSet where ((prices.InstrumentId == instrument.Id) && (timestamp > prices.Date)) orderby prices.Date ascending select prices).FirstOrDefault();
                DataMaker.TradeRecorder(bs, quantity, quantity * price, timestamp, markprice.InstrumentId, quantity * (markprice.ClosingPrice - Convert.ToDouble(markprice.MarkPrice == null ? 0.0 : markprice.MarkPrice)), markprice.Id);
            }
            catch
            {
            }
            refresh_view();
        }
        static public void refresh_view()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            tradeList = new List<object>();
            var trades = from tradeset in context.TradeSet join priceset in context.PriceSet on tradeset.UniquePrice.Id equals priceset.Id orderby tradeset.Id select new { ID = tradeset.Id, IsBuy = tradeset.IsBuy, Quantity = tradeset.Quantity, Instrument = tradeset.Instrument.Ticker, Type = tradeset.Instrument.InstType.TypeName, TradePrice = tradeset.Price, MarkPrice = priceset.MarkPrice, PNL = tradeset.PNL, Delta = priceset.Delta, Gamma = priceset.Gamma, Theta = priceset.Theta , Vega = priceset.Vega, Rho = priceset.Rho };
            foreach (var trade in trades)
            {
                tradeList.Add(trade);
            }
            try
            {
                MainForm.dataGridViewAllTrade.DataSource = tradeList;
            }
            catch
            {
            }
            MainForm.refresh(true);
            MainForm.refresh(false);
        }
        static public void update_trade(Price price, Boolean oldOrNew)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            Price record_price = new Price();
            try
            {
               record_price  = (from priceset in context.PriceSet where (priceset.Instrument.Ticker == price.Instrument.Ticker && priceset.Date < price.Date) orderby priceset.Date descending select priceset).FirstOrDefault();
            }
            catch
            {
                var unsupported_trades = from tradeset in context.TradeSet where tradeset.PriceId == price.Id select tradeset;
                context.TradeSet.RemoveRange(unsupported_trades);
                return;
            }
            Price last_price = oldOrNew ? price : record_price;
            Price new_price = oldOrNew ? record_price : price;
            foreach (Trade trades in last_price.Trades)
            {
                if (trades.TimeStamp > new_price.Date)
                {
                    Trade new_trade = new Trade();
                    new_trade.Id = trades.Id;
                    context.TradeSet.Attach(new_trade);
                    new_trade.InstrumentId = trades.InstrumentId;
                    new_trade.IsBuy = trades.IsBuy;
                    new_trade.Quantity = trades.Quantity;
                    new_trade.TimeStamp = trades.TimeStamp;
                    new_trade.PriceId = new_price.Id;
                    int direction = trades.IsBuy ? -1 : 1;
                    new_trade.PNL = direction * (trades.Price - new_price.ClosingPrice);
                    context.SaveChanges();
                }
            }
            refresh_view();
        }
    }
}
