﻿namespace Monte_Model
{
    partial class SettingFormcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCV = new System.Windows.Forms.Label();
            this.labelMultiThread = new System.Windows.Forms.Label();
            this.labelTrails = new System.Windows.Forms.Label();
            this.labelAnti = new System.Windows.Forms.Label();
            this.labelSteps = new System.Windows.Forms.Label();
            this.comboBoxAnti = new System.Windows.Forms.ComboBox();
            this.comboBoxCV = new System.Windows.Forms.ComboBox();
            this.comboBoxMultiThread = new System.Windows.Forms.ComboBox();
            this.textBoxMultiThread = new System.Windows.Forms.TextBox();
            this.textBoxTrails = new System.Windows.Forms.TextBox();
            this.textBoxSteps = new System.Windows.Forms.TextBox();
            this.buttonSaveChange = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCV
            // 
            this.labelCV.AutoSize = true;
            this.labelCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCV.Location = new System.Drawing.Point(28, 104);
            this.labelCV.Name = "labelCV";
            this.labelCV.Size = new System.Drawing.Size(142, 25);
            this.labelCV.TabIndex = 12;
            this.labelCV.Text = "Control Variant";
            // 
            // labelMultiThread
            // 
            this.labelMultiThread.AutoSize = true;
            this.labelMultiThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMultiThread.Location = new System.Drawing.Point(28, 149);
            this.labelMultiThread.Name = "labelMultiThread";
            this.labelMultiThread.Size = new System.Drawing.Size(123, 25);
            this.labelMultiThread.TabIndex = 11;
            this.labelMultiThread.Text = "Multi-Thread";
            // 
            // labelTrails
            // 
            this.labelTrails.AutoSize = true;
            this.labelTrails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTrails.Location = new System.Drawing.Point(28, 196);
            this.labelTrails.Name = "labelTrails";
            this.labelTrails.Size = new System.Drawing.Size(134, 25);
            this.labelTrails.TabIndex = 10;
            this.labelTrails.Text = "General Trails";
            // 
            // labelAnti
            // 
            this.labelAnti.AutoSize = true;
            this.labelAnti.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAnti.Location = new System.Drawing.Point(28, 59);
            this.labelAnti.Name = "labelAnti";
            this.labelAnti.Size = new System.Drawing.Size(163, 25);
            this.labelAnti.TabIndex = 9;
            this.labelAnti.Text = "Antithetic Method";
            // 
            // labelSteps
            // 
            this.labelSteps.AutoSize = true;
            this.labelSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSteps.Location = new System.Drawing.Point(28, 241);
            this.labelSteps.Name = "labelSteps";
            this.labelSteps.Size = new System.Drawing.Size(137, 25);
            this.labelSteps.TabIndex = 13;
            this.labelSteps.Text = "General Steps";
            // 
            // comboBoxAnti
            // 
            this.comboBoxAnti.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAnti.FormattingEnabled = true;
            this.comboBoxAnti.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.comboBoxAnti.Location = new System.Drawing.Point(223, 56);
            this.comboBoxAnti.Name = "comboBoxAnti";
            this.comboBoxAnti.Size = new System.Drawing.Size(159, 33);
            this.comboBoxAnti.TabIndex = 14;
            // 
            // comboBoxCV
            // 
            this.comboBoxCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCV.FormattingEnabled = true;
            this.comboBoxCV.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.comboBoxCV.Location = new System.Drawing.Point(223, 101);
            this.comboBoxCV.Name = "comboBoxCV";
            this.comboBoxCV.Size = new System.Drawing.Size(159, 33);
            this.comboBoxCV.TabIndex = 15;
            // 
            // comboBoxMultiThread
            // 
            this.comboBoxMultiThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMultiThread.FormattingEnabled = true;
            this.comboBoxMultiThread.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.comboBoxMultiThread.Location = new System.Drawing.Point(223, 146);
            this.comboBoxMultiThread.Name = "comboBoxMultiThread";
            this.comboBoxMultiThread.Size = new System.Drawing.Size(77, 33);
            this.comboBoxMultiThread.TabIndex = 16;
            // 
            // textBoxMultiThread
            // 
            this.textBoxMultiThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMultiThread.Location = new System.Drawing.Point(306, 147);
            this.textBoxMultiThread.Name = "textBoxMultiThread";
            this.textBoxMultiThread.Size = new System.Drawing.Size(76, 32);
            this.textBoxMultiThread.TabIndex = 17;
            // 
            // textBoxTrails
            // 
            this.textBoxTrails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTrails.Location = new System.Drawing.Point(223, 191);
            this.textBoxTrails.Name = "textBoxTrails";
            this.textBoxTrails.Size = new System.Drawing.Size(159, 30);
            this.textBoxTrails.TabIndex = 18;
            // 
            // textBoxSteps
            // 
            this.textBoxSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSteps.Location = new System.Drawing.Point(223, 236);
            this.textBoxSteps.Name = "textBoxSteps";
            this.textBoxSteps.Size = new System.Drawing.Size(159, 30);
            this.textBoxSteps.TabIndex = 19;
            // 
            // buttonSaveChange
            // 
            this.buttonSaveChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveChange.Location = new System.Drawing.Point(33, 276);
            this.buttonSaveChange.Name = "buttonSaveChange";
            this.buttonSaveChange.Size = new System.Drawing.Size(349, 52);
            this.buttonSaveChange.TabIndex = 20;
            this.buttonSaveChange.Text = "SAVE CHANGES";
            this.buttonSaveChange.UseVisualStyleBackColor = true;
            this.buttonSaveChange.Click += new System.EventHandler(this.buttonSaveChange_Click);
            // 
            // SettingFormcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 347);
            this.Controls.Add(this.buttonSaveChange);
            this.Controls.Add(this.textBoxSteps);
            this.Controls.Add(this.textBoxTrails);
            this.Controls.Add(this.textBoxMultiThread);
            this.Controls.Add(this.comboBoxMultiThread);
            this.Controls.Add(this.comboBoxCV);
            this.Controls.Add(this.comboBoxAnti);
            this.Controls.Add(this.labelSteps);
            this.Controls.Add(this.labelCV);
            this.Controls.Add(this.labelMultiThread);
            this.Controls.Add(this.labelTrails);
            this.Controls.Add(this.labelAnti);
            this.Name = "SettingFormcs";
            this.Text = "SettingFormcs";
            this.Load += new System.EventHandler(this.SettingFormcs_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCV;
        private System.Windows.Forms.Label labelMultiThread;
        private System.Windows.Forms.Label labelTrails;
        private System.Windows.Forms.Label labelAnti;
        private System.Windows.Forms.Label labelSteps;
        private System.Windows.Forms.ComboBox comboBoxAnti;
        private System.Windows.Forms.ComboBox comboBoxCV;
        private System.Windows.Forms.ComboBox comboBoxMultiThread;
        private System.Windows.Forms.TextBox textBoxMultiThread;
        private System.Windows.Forms.TextBox textBoxTrails;
        private System.Windows.Forms.TextBox textBoxSteps;
        private System.Windows.Forms.Button buttonSaveChange;
    }
}