﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimulatorClass;

namespace Monte_Model
{
    class DataMaker
    {
        static public Boolean TradeRecorder(Boolean isBuy, double quantity, double price, DateTime timeStamp, int instrumentId, double pNL, int priceId)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                Trade trade = new Trade { IsBuy = isBuy, Quantity = quantity, Price = price, TimeStamp = timeStamp, InstrumentId = instrumentId, PNL = pNL, PriceId = priceId };
                context.TradeSet.Add(trade);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static public Boolean InstrumentRecorder(string companyName, string ticker, string exchange, Nullable<int> underlying, Nullable<double> strike, double tenor, Nullable<Boolean> isCall, int instTypeId, Nullable<double> rebate, Nullable<double> barrier, string barrierType, DateTime startDate)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                Instrument instrument = new Instrument { CompanyName = companyName, Ticker = ticker, Exchange = exchange, Underlying = underlying, Strike = strike, Tenor = tenor, IsCall = isCall, InstTypeId = instTypeId, Rebate = rebate, Barrier = barrier, BarrierType = barrierType, StartDate = startDate };
                context.InstrumentSet.Add(instrument);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static public Boolean PriceRecorder(DateTime date, double closingPrice, int instrumentId, Nullable<double> markPrice, Nullable<double> delta, Nullable<double> gamma, Nullable<double> theta, Nullable<double> vega, Nullable<double> rho)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                Price price = new Price { Date = date, ClosingPrice = closingPrice, InstrumentId = instrumentId, MarkPrice = markPrice, Delta = delta, Gamma = gamma, Theta = theta, Vega = vega, Rho = rho };
                context.PriceSet.Add(price);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static public Boolean InstTypeRecorder(string typeName)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                InstType instType = new InstType { TypeName = typeName };
                context.InstTypeSet.Add(instType);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static public Boolean InterestRateRecorder(double tenor, double rate)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                InterestRate interestRate = new InterestRate { Tenor = tenor, Rate = rate };
                context.InterestRateSet.Add(interestRate);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static public Boolean InstTypePre()
        {
            InstTypeRecorder("Stock");
            InstTypeRecorder("European");
            InstTypeRecorder("Range");
            InstTypeRecorder("Lookback");
            InstTypeRecorder("Asian");
            InstTypeRecorder("Digital");
            InstTypeRecorder("Barrier");
            return true;
        }
    }
}