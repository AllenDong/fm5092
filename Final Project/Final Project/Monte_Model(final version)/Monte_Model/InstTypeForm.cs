﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class InstTypeForm : Form
    {
        static public List<object> instTypeList;
        public InstTypeForm()
        {
            InitializeComponent();
            refresh_view();
        }
        private void InstTypeForm_Load(object sender, EventArgs e)
        {
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string typeName = textBoxTypeName.Text;
                DataMaker.InstTypeRecorder(typeName);
            }
            catch
            {
            }
            refresh_view();
        }
        static public void refresh_view()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            instTypeList = new List<object>();
            var types = from typeset in context.InstTypeSet orderby typeset.Id select new { ID = typeset.Id, Name = typeset.TypeName };
            foreach (var type in types)
            {
                instTypeList.Add(type);
            }
            try
            {
                dataGridViewInstType.DataSource = instTypeList;
                dataGridViewInstType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch
            {
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            DataGridViewSelectedRowCollection selected_rows = dataGridViewInstType.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["ID"].Value);
                InstType instType = (from types in context.InstTypeSet where types.Id == id select types).FirstOrDefault();
                try
                {
                    context.InstTypeSet.Remove(instType);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
            refresh_view();
        }
        private void buttonPreLoad_Click(object sender, EventArgs e)
        {
            DataMaker.InstTypePre();
            refresh_view();
        }
    }
}
