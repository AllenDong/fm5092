﻿using System.Drawing;

namespace Monte_Model
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalPriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshTradesFromDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceBookUsingSimulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            dataGridViewTotalView = new System.Windows.Forms.DataGridView();
            this.textBoxVolatility = new System.Windows.Forms.TextBox();
            this.label_volatility = new System.Windows.Forms.Label();
            this.label_totals = new System.Windows.Forms.Label();
            this.label_allTrade = new System.Windows.Forms.Label();
            dataGridViewAllTrade = new System.Windows.Forms.DataGridView();
            this.labelMultiSelect = new System.Windows.Forms.Label();
            dataGridViewMultiSelect = new System.Windows.Forms.DataGridView();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewTotalView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewAllTrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewMultiSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.analysisToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1307, 32);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.refreshTradesFromDatabaseToolStripMenuItem,
            this.priceBookUsingSimulationToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 28);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instrumentTypeToolStripMenuItem,
            this.instrumentToolStripMenuItem,
            this.tradeToolStripMenuItem,
            this.interestRateToolStripMenuItem,
            this.historicalPriceToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(345, 30);
            this.newToolStripMenuItem.Text = "New...";
            // 
            // instrumentTypeToolStripMenuItem
            // 
            this.instrumentTypeToolStripMenuItem.Name = "instrumentTypeToolStripMenuItem";
            this.instrumentTypeToolStripMenuItem.Size = new System.Drawing.Size(233, 30);
            this.instrumentTypeToolStripMenuItem.Text = "Instrument Type";
            this.instrumentTypeToolStripMenuItem.Click += new System.EventHandler(this.instrumentTypeToolStripMenuItem_Click);
            // 
            // instrumentToolStripMenuItem
            // 
            this.instrumentToolStripMenuItem.Name = "instrumentToolStripMenuItem";
            this.instrumentToolStripMenuItem.Size = new System.Drawing.Size(233, 30);
            this.instrumentToolStripMenuItem.Text = "Instrument";
            this.instrumentToolStripMenuItem.Click += new System.EventHandler(this.instrumentToolStripMenuItem_Click);
            // 
            // tradeToolStripMenuItem
            // 
            this.tradeToolStripMenuItem.Name = "tradeToolStripMenuItem";
            this.tradeToolStripMenuItem.Size = new System.Drawing.Size(233, 30);
            this.tradeToolStripMenuItem.Text = "Trade";
            this.tradeToolStripMenuItem.Click += new System.EventHandler(this.tradeToolStripMenuItem_Click);
            // 
            // interestRateToolStripMenuItem
            // 
            this.interestRateToolStripMenuItem.Name = "interestRateToolStripMenuItem";
            this.interestRateToolStripMenuItem.Size = new System.Drawing.Size(233, 30);
            this.interestRateToolStripMenuItem.Text = "Interest Rate";
            this.interestRateToolStripMenuItem.Click += new System.EventHandler(this.interestRateToolStripMenuItem_Click);
            // 
            // historicalPriceToolStripMenuItem
            // 
            this.historicalPriceToolStripMenuItem.Name = "historicalPriceToolStripMenuItem";
            this.historicalPriceToolStripMenuItem.Size = new System.Drawing.Size(233, 30);
            this.historicalPriceToolStripMenuItem.Text = "Historical Price";
            this.historicalPriceToolStripMenuItem.Click += new System.EventHandler(this.historicalPriceToolStripMenuItem_Click);
            // 
            // refreshTradesFromDatabaseToolStripMenuItem
            // 
            this.refreshTradesFromDatabaseToolStripMenuItem.Name = "refreshTradesFromDatabaseToolStripMenuItem";
            this.refreshTradesFromDatabaseToolStripMenuItem.Size = new System.Drawing.Size(345, 30);
            this.refreshTradesFromDatabaseToolStripMenuItem.Text = "Refresh trades from database";
            // 
            // priceBookUsingSimulationToolStripMenuItem
            // 
            this.priceBookUsingSimulationToolStripMenuItem.Name = "priceBookUsingSimulationToolStripMenuItem";
            this.priceBookUsingSimulationToolStripMenuItem.Size = new System.Drawing.Size(345, 30);
            this.priceBookUsingSimulationToolStripMenuItem.Text = "Price book using simulation";
            // 
            // analysisToolStripMenuItem
            // 
            this.analysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instrumentAnalysisToolStripMenuItem,
            this.interestRateToolStripMenuItem1});
            this.analysisToolStripMenuItem.Name = "analysisToolStripMenuItem";
            this.analysisToolStripMenuItem.Size = new System.Drawing.Size(92, 28);
            this.analysisToolStripMenuItem.Text = "&Analysis";
            // 
            // instrumentAnalysisToolStripMenuItem
            // 
            this.instrumentAnalysisToolStripMenuItem.Name = "instrumentAnalysisToolStripMenuItem";
            this.instrumentAnalysisToolStripMenuItem.Size = new System.Drawing.Size(261, 30);
            this.instrumentAnalysisToolStripMenuItem.Text = "Instrument Analysis";
            this.instrumentAnalysisToolStripMenuItem.Click += new System.EventHandler(this.instrumentAnalysisToolStripMenuItem_Click);
            // 
            // interestRateToolStripMenuItem1
            // 
            this.interestRateToolStripMenuItem1.Name = "interestRateToolStripMenuItem1";
            this.interestRateToolStripMenuItem1.Size = new System.Drawing.Size(261, 30);
            this.interestRateToolStripMenuItem1.Text = "Interest Rate";
            this.interestRateToolStripMenuItem1.Click += new System.EventHandler(this.interestRateToolStripMenuItem1_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(92, 28);
            this.settingsToolStripMenuItem.Text = "&Settings";
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(149, 30);
            this.configToolStripMenuItem.Text = "Config";
            // 
            // dataGridViewTotalView
            // 
            dataGridViewTotalView.AllowUserToOrderColumns = true;
            dataGridViewTotalView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewTotalView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewTotalView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewTotalView.GridColor = System.Drawing.SystemColors.Control;
            dataGridViewTotalView.Location = new System.Drawing.Point(31, 103);
            dataGridViewTotalView.Name = "dataGridViewTotalView";
            dataGridViewTotalView.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewTotalView.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewTotalView.RowHeadersVisible = false;
            dataGridViewTotalView.RowTemplate.Height = 28;
            dataGridViewTotalView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewTotalView.Size = new System.Drawing.Size(1242, 126);
            dataGridViewTotalView.TabIndex = 1;
            dataGridViewTotalView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TotalView_CellContentClick);
            // 
            // textBoxVolatility
            // 
            this.textBoxVolatility.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxVolatility.Location = new System.Drawing.Point(208, 42);
            this.textBoxVolatility.Name = "textBoxVolatility";
            this.textBoxVolatility.Size = new System.Drawing.Size(124, 30);
            this.textBoxVolatility.TabIndex = 2;
            this.textBoxVolatility.Text = "0.5";
            // 
            // label_volatility
            // 
            this.label_volatility.AutoSize = true;
            this.label_volatility.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_volatility.Location = new System.Drawing.Point(27, 47);
            this.label_volatility.Name = "label_volatility";
            this.label_volatility.Size = new System.Drawing.Size(175, 22);
            this.label_volatility.TabIndex = 3;
            this.label_volatility.Text = "Pricing Volatility (%):";
            // 
            // label_totals
            // 
            this.label_totals.AutoSize = true;
            this.label_totals.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_totals.Location = new System.Drawing.Point(27, 78);
            this.label_totals.Name = "label_totals";
            this.label_totals.Size = new System.Drawing.Size(65, 22);
            this.label_totals.TabIndex = 4;
            this.label_totals.Text = "Totals:";
            // 
            // label_allTrade
            // 
            this.label_allTrade.AutoSize = true;
            this.label_allTrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_allTrade.Location = new System.Drawing.Point(27, 374);
            this.label_allTrade.Name = "label_allTrade";
            this.label_allTrade.Size = new System.Drawing.Size(97, 22);
            this.label_allTrade.TabIndex = 5;
            this.label_allTrade.Text = "All Trades:";
            // 
            // dataGridViewAllTrade
            // 
            dataGridViewAllTrade.AllowUserToOrderColumns = true;
            dataGridViewAllTrade.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewAllTrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewAllTrade.GridColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewAllTrade.Location = new System.Drawing.Point(31, 399);
            dataGridViewAllTrade.Name = "dataGridViewAllTrade";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewAllTrade.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewAllTrade.RowHeadersVisible = false;
            dataGridViewAllTrade.RowTemplate.Height = 28;
            dataGridViewAllTrade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewAllTrade.Size = new System.Drawing.Size(1242, 230);
            dataGridViewAllTrade.TabIndex = 6;
            dataGridViewAllTrade.SelectionChanged += new System.EventHandler(dataGridViewAllTrade_CellContentClick);
            // 
            // labelMultiSelect
            // 
            this.labelMultiSelect.AutoSize = true;
            this.labelMultiSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMultiSelect.Location = new System.Drawing.Point(27, 232);
            this.labelMultiSelect.Name = "labelMultiSelect";
            this.labelMultiSelect.Size = new System.Drawing.Size(151, 22);
            this.labelMultiSelect.TabIndex = 7;
            this.labelMultiSelect.Text = "Multiple Selected:";
            // 
            // dataGridViewMultiSelect
            // 
            dataGridViewMultiSelect.AllowUserToOrderColumns = true;
            dataGridViewMultiSelect.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewMultiSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewMultiSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewMultiSelect.GridColor = System.Drawing.SystemColors.Control;
            dataGridViewMultiSelect.Location = new System.Drawing.Point(31, 257);
            dataGridViewMultiSelect.Name = "dataGridViewMultiSelect";
            dataGridViewMultiSelect.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewMultiSelect.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewMultiSelect.RowHeadersVisible = false;
            dataGridViewMultiSelect.RowTemplate.Height = 28;
            dataGridViewMultiSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewMultiSelect.Size = new System.Drawing.Size(1242, 114);
            dataGridViewMultiSelect.TabIndex = 8;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(1096, 651);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(177, 34);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "DELETE";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1307, 697);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(dataGridViewMultiSelect);
            this.Controls.Add(this.labelMultiSelect);
            this.Controls.Add(dataGridViewAllTrade);
            this.Controls.Add(this.label_allTrade);
            this.Controls.Add(this.label_totals);
            this.Controls.Add(this.label_volatility);
            this.Controls.Add(this.textBoxVolatility);
            this.Controls.Add(dataGridViewTotalView);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "My Portfolio Manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewTotalView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewAllTrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewMultiSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historicalPriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshTradesFromDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceBookUsingSimulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxVolatility;
        private System.Windows.Forms.Label label_volatility;
        private System.Windows.Forms.Label label_totals;
        private System.Windows.Forms.Label label_allTrade;
        private System.Windows.Forms.ToolStripMenuItem instrumentAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.Label labelMultiSelect;
        private System.Windows.Forms.Button buttonDelete;
        static public System.Windows.Forms.DataGridView dataGridViewTotalView = new System.Windows.Forms.DataGridView();
        static public System.Windows.Forms.DataGridView dataGridViewAllTrade = new System.Windows.Forms.DataGridView();
        static public System.Windows.Forms.DataGridView dataGridViewMultiSelect = new System.Windows.Forms.DataGridView();
    }
}

