﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class InstrumentForm : Form
    {
        public Control[] control_list;
        static public List<object> instrumentList;
        public InstrumentForm()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            InitializeComponent();
            refresh_view();
            comboBoxInstType.Items.Clear();
            foreach (var insttype in context.InstTypeSet) { comboBoxInstType.Items.Add(insttype.TypeName); }
            control_list = new Control[] { comboBoxUnderlying, textBoxTenor, comboBoxBarrierType, textBoxBarrier, textBoxRebate, comboBoxCP, textBoxStrike };
        }
        private void InstrumentForm_Load(object sender, EventArgs e)
        {
        }
        private void buttonSaveInst_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                #region
                string companyname = textBoxCompanyName.Text;
                string ticker = textBoxTicker.Text;
                string exchange = textBoxExchange.Text;
                double tenor = (textBoxTenor.Text == "" ? 0 : Convert.ToDouble(textBoxTenor.Text));
                int insttypeid = (from insttypes in context.InstTypeSet where (insttypes.TypeName == comboBoxInstType.Text) orderby insttypes.TypeName select insttypes).FirstOrDefault().Id;
                string barriertype = comboBoxBarrierType.Text;
                Nullable<int> underlying;
                Nullable<double> strike;
                Nullable<Boolean> cp;
                Nullable<double> rebate;
                Nullable<double> barrier;
                if (comboBoxUnderlying.Text == "") { underlying = null; } else { underlying = Convert.ToInt32((from instrument in context.InstrumentSet where instrument.Ticker == comboBoxUnderlying.Text select instrument).FirstOrDefault().Id); }
                if (textBoxStrike.Text == "") { strike = null; } else { strike = Convert.ToDouble(textBoxStrike.Text); }
                if (comboBoxCP.Text == "") { cp = null; } else { cp = (comboBoxCP.Text == "Call"); }
                if (textBoxRebate.Text == "") { rebate = null; } else { rebate = Convert.ToDouble(textBoxRebate.Text); }
                if (textBoxBarrier.Text == "") { barrier = null; } else { barrier = Convert.ToDouble(textBoxBarrier.Text); }
                #endregion
                DateTime startdate = dateTimePickerTimeStamp.Value;
                DataMaker.InstrumentRecorder(companyname, ticker, exchange, underlying, strike, tenor, cp, insttypeid, rebate, barrier, barriertype, startdate);
            }
            catch
            {
            }
            refresh_view();
        }
        static public void refresh_view()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            comboBoxUnderlying.Items.Clear();
            foreach (var instrumentset in context.InstrumentSet) { if (instrumentset.InstType.TypeName == "Stock") { comboBoxUnderlying.Items.Add(instrumentset.Ticker); } }
            instrumentList =new List<object>();
            var instruments = from instrumentset in context.InstrumentSet join insttypeset in context.InstTypeSet on instrumentset.InstTypeId equals insttypeset.Id orderby instrumentset.Id select new { ID = instrumentset.Id, Ticker = instrumentset.Ticker, Type = insttypeset.TypeName, StartDate = instrumentset.StartDate, Company = instrumentset.CompanyName, Exchange = instrumentset.Exchange, Underlying = instrumentset.Underlying, Tenor = instrumentset.Tenor, IsCall = instrumentset.IsCall, Strike = instrumentset.Strike, BarrierType = instrumentset.BarrierType, Barrier = instrumentset.Barrier, Rebate = instrumentset.Rebate};
            foreach (var instrument in instruments)
            {
                instrumentList.Add(instrument);
            }
            try
            {
                dataGridViewInstrument.DataSource = instrumentList;
            }
            catch
            {
            }
        }
        private void comboBoxInstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxInstType.Text)
            {
                case "European":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = true;
                    textBoxStrike.Enabled = true;
                    break;
                case "Asian":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = true;
                    textBoxStrike.Enabled = true;
                    break;
                case "Lookback":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = true;
                    textBoxStrike.Enabled = true;
                    break;
                case "Range":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = false;
                    textBoxStrike.Enabled = false;
                    break;
                case "Digital":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = true;
                    comboBoxCP.Enabled = true;
                    textBoxStrike.Enabled = true;
                    break;
                case "Barrier":
                    comboBoxUnderlying.Enabled = true;
                    textBoxTenor.Enabled = true;
                    comboBoxBarrierType.Enabled = true;
                    textBoxBarrier.Enabled = true;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = true;
                    textBoxStrike.Enabled = true;
                    break;
                case "Stock":
                    comboBoxUnderlying.Enabled = false;
                    textBoxTenor.Enabled = false;
                    comboBoxBarrierType.Enabled = false;
                    textBoxBarrier.Enabled = false;
                    textBoxRebate.Enabled = false;
                    comboBoxCP.Enabled = false;
                    textBoxStrike.Enabled = false;
                    break;
            }
            foreach (Control control in control_list)
            {
                if (!control.Enabled)
                {
                    control.Text = "";
                }
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            DataGridViewSelectedRowCollection selected_rows = dataGridViewInstrument.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["ID"].Value);
                Price price = (from prices in context.PriceSet where prices.Id == id select prices).FirstOrDefault();
                try
                {
                    context.PriceSet.Remove(price);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
            refresh_view();
        }
    }
}
