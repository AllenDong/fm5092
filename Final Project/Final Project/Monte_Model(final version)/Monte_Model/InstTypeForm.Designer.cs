﻿namespace Monte_Model
{
    partial class InstTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            dataGridViewInstType = new System.Windows.Forms.DataGridView();
            this.instTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxTypeName = new System.Windows.Forms.TextBox();
            this.labelTypeName = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonPreLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInstType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewInstType
            // 
            dataGridViewInstType.AllowUserToOrderColumns = true;
            dataGridViewInstType.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewInstType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewInstType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewInstType.GridColor = System.Drawing.SystemColors.Control;
            dataGridViewInstType.Location = new System.Drawing.Point(27, 101);
            dataGridViewInstType.Name = "dataGridViewInstType";
            dataGridViewInstType.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewInstType.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewInstType.RowHeadersVisible = false;
            dataGridViewInstType.RowTemplate.Height = 28;
            dataGridViewInstType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewInstType.Size = new System.Drawing.Size(435, 216);
            dataGridViewInstType.TabIndex = 9;
            // 
            // instTypeBindingSource
            // 
            this.instTypeBindingSource.DataSource = typeof(Monte_Model.InstType);
            // 
            // textBoxTypeName
            // 
            this.textBoxTypeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTypeName.Location = new System.Drawing.Point(118, 21);
            this.textBoxTypeName.Name = "textBoxTypeName";
            this.textBoxTypeName.Size = new System.Drawing.Size(200, 26);
            this.textBoxTypeName.TabIndex = 29;
            // 
            // labelTypeName
            // 
            this.labelTypeName.AutoSize = true;
            this.labelTypeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTypeName.Location = new System.Drawing.Point(23, 24);
            this.labelTypeName.Name = "labelTypeName";
            this.labelTypeName.Size = new System.Drawing.Size(89, 20);
            this.labelTypeName.TabIndex = 28;
            this.labelTypeName.Text = "Type Name";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.Location = new System.Drawing.Point(350, 21);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(112, 30);
            this.buttonAdd.TabIndex = 30;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(384, 20);
            this.label1.TabIndex = 31;
            this.label1.Text = "No effect to trading, please contact the administrator!";
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.Location = new System.Drawing.Point(296, 323);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(166, 30);
            this.buttonDelete.TabIndex = 32;
            this.buttonDelete.Text = "DELETE";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonPreLoad
            // 
            this.buttonPreLoad.Location = new System.Drawing.Point(27, 323);
            this.buttonPreLoad.Name = "buttonPreLoad";
            this.buttonPreLoad.Size = new System.Drawing.Size(241, 30);
            this.buttonPreLoad.TabIndex = 33;
            this.buttonPreLoad.Text = "Pre-Load required Types";
            this.buttonPreLoad.UseVisualStyleBackColor = true;
            this.buttonPreLoad.Click += new System.EventHandler(this.buttonPreLoad_Click);
            // 
            // InstTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 379);
            this.Controls.Add(this.buttonPreLoad);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxTypeName);
            this.Controls.Add(this.labelTypeName);
            this.Controls.Add(dataGridViewInstType);
            this.Name = "InstTypeForm";
            this.Text = "InstTypeForm";
            this.Load += new System.EventHandler(this.InstTypeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInstType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxTypeName;
        private System.Windows.Forms.Label labelTypeName;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource instTypeBindingSource;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonPreLoad;
        static private System.Windows.Forms.DataGridView dataGridViewInstType = new System.Windows.Forms.DataGridView();
    }
}