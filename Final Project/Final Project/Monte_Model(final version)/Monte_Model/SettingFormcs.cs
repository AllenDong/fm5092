﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class SettingFormcs : Form
    {
        static public SimulatorClass.Method_Type[] Method_Type;
        static public int trails;
        static public int steps;
        public SettingFormcs()
        {
            InitializeComponent();
        }
        private void SettingFormcs_Load(object sender, EventArgs e)
        {

        }
        private void buttonSaveChange_Click(object sender, EventArgs e)
        {

        }
        static public void initial()
        {
            Method_Type = new SimulatorClass.Method_Type[] { SimulatorClass.Method_Type.Antithetic, SimulatorClass.Method_Type.Control_Variance, SimulatorClass.Method_Type.Multi_Thread };
            trails = 100000;
            steps = 10;
        }
    }
}
