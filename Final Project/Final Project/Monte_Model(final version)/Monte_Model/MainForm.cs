﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class MainForm : Form
    {
        static public double volatility;
        public MainForm()
        {
            InitializeComponent();
            refreshComponent();
            SettingFormcs.initial();
            refresh(true);
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
        }
        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void TotalView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstTypeForm instTypeForm = new InstTypeForm();
            instTypeForm.ShowDialog();
        }
        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstrumentForm instrumentForm = new InstrumentForm();
            instrumentForm.ShowDialog();
        }
        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TradeForm tradeForm = new TradeForm();
            tradeForm.ShowDialog();
        }
        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InterestRateForm interestRateForm = new InterestRateForm();
            interestRateForm.ShowDialog();
        }
        private void historicalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoricalPriceForm historicalPriceForm = new HistoricalPriceForm();
            volatility = Convert.ToDouble(textBoxVolatility.Text);
            historicalPriceForm.ShowDialog();
        }
        private void refreshComponent()
        {
            InstrumentForm.refresh_view();
            InstTypeForm.refresh_view();
            InterestRateForm.refresh_view();
            HistoricalPriceForm.refresh_view();
            TradeForm.refresh_view();
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            DataGridViewSelectedRowCollection selected_rows = dataGridViewAllTrade.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["ID"].Value);
                Trade trade = (from trades in context.TradeSet where trades.Id == id select trades).FirstOrDefault();
                try
                {
                    context.TradeSet.Remove(trade);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
            TradeForm.refresh_view();
        }
        static public void refresh(Boolean isAll)
        {
            var selected_rows = dataGridViewAllTrade.Rows;
            double price = 0;
            double quantity = 0;
            double markprice = 0;
            double delta = 0;
            double gamma = 0;
            double theta = 0;
            double vega = 0;
            double rho = 0;
            double PnL = 0;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                if (selected_rows[idx].Selected || isAll)
                {
                    int direction = Convert.ToBoolean(selected_rows[idx].Cells["IsBuy"].Value) ? 1 : -1;
                    double quantity_single = Convert.ToDouble(selected_rows[idx].Cells["Quantity"].Value) * direction;
                    quantity = quantity + quantity_single;
                    price = price + Convert.ToDouble(selected_rows[idx].Cells["TradePrice"].Value) * quantity_single;
                    markprice = markprice + Convert.ToDouble(selected_rows[idx].Cells["MarkPrice"].Value) * quantity_single;
                    PnL = PnL + Convert.ToDouble(selected_rows[idx].Cells["PNL"].Value) * quantity_single;
                    delta = delta + Convert.ToDouble(selected_rows[idx].Cells["Delta"].Value) * quantity_single;
                    gamma = gamma + Convert.ToDouble(selected_rows[idx].Cells["Gamma"].Value) * quantity_single;
                    theta = theta + Convert.ToDouble(selected_rows[idx].Cells["Theta"].Value) * quantity_single;
                    vega = vega + Convert.ToDouble(selected_rows[idx].Cells["Vega"].Value) * quantity_single;
                    rho = rho + Convert.ToDouble(selected_rows[idx].Cells["Rho"].Value) * quantity_single;
                }
            }
            List<object> tradesList = new List<object> { new { Quantity = quantity, Price = price, MarkPrice = markprice, PNL = PnL, Delta = delta, Gamma = gamma, Theta = theta, Vega = vega, Rho = rho } };
            if (isAll)
            {
                dataGridViewTotalView.DataSource = tradesList;
            }
            else
                dataGridViewMultiSelect.DataSource = tradesList;
        }
        private void dataGridViewAllTrade_CellContentClick(object sender, EventArgs e)
        {
            refresh(false);
        }
        private void instrumentAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PriceAnalysisForm priceAnalysisForm = new PriceAnalysisForm();
            priceAnalysisForm.ShowDialog();
        }
        private void interestRateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InterestAnalysis interestAnalysis = new InterestAnalysis();
            interestAnalysis.ShowDialog();
        }
    }
}
