﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class InterestAnalysis : Form
    {
        public InterestAnalysis()
        {
            InitializeComponent();
            refresh_chart();
        }
        private void InterestAnalysis_Load(object sender, EventArgs e)
        {
        }
        public void refresh_chart()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                var interests = from interestset in context.InterestRateSet select interestset;
                List<double> x_axis = new List<double>();
                List<double> y_axis = new List<double>();
                foreach (InterestRate interest in interests)
                {
                    x_axis.Add(interest.Tenor);
                    y_axis.Add(interest.Rate);
                }
                chartInterestRate.Series[0].Points.DataBindXY(x_axis, y_axis);
            }
            catch
            {
            }
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            double rate = ToolFunctions.generate_interest_rate(Convert.ToDouble(textBoxTenor.Text));
            textBoxRate.Text = Convert.ToString(rate);
        }
    }
}
