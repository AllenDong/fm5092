﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class HistoricalPriceForm : Form
    {
        static public double rebate_barrier = 0;
        static public List<object> priceList;
        public HistoricalPriceForm()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            InitializeComponent();
            refresh_view();
            foreach (var instruments in context.InstrumentSet) { InstNameList.Items.Add(instruments.Ticker); }
        }
        private void HistoricalPriceForm_Load(object sender, EventArgs e)
        {
        }
        private void buttonADD_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                DateTime date = HistoryDate.Value;
                double closingprice = Convert.ToDouble(textBoxClosingPrice.Text);
                Instrument instrument = (from instruments in context.InstrumentSet where instruments.Ticker == InstNameList.SelectedItem.ToString() select instruments).FirstOrDefault();
                Instrument underlying = (from underlyings in context.InstrumentSet where underlyings.Id == instrument.Underlying orderby underlyings.StartDate descending select underlyings).FirstOrDefault();
                Price underlyingprice;
                double spot;
                if (instrument.InstType.TypeName != "Stock")
                {
                    underlyingprice = (from prices in context.PriceSet where prices.InstrumentId == underlying.Id orderby prices.Date descending select prices).FirstOrDefault();
                    spot = underlyingprice.ClosingPrice;
                }
                else
                {
                    spot = closingprice;
                }
                double tenor = instrument.Tenor - (date.ToOADate() - instrument.StartDate.ToOADate()) / 365;
                double strike = (instrument.Strike == null ? 0 : Convert.ToDouble(instrument.Strike));
                SimulatorClass.Monte_Simulator.Monte_Simulate(ToolFunctions.InstrumentType(instrument), spot, strike, ToolFunctions.generate_interest_rate(tenor), MainForm.volatility, tenor, SettingFormcs.trails, SettingFormcs.steps, ToolFunctions.option_Type(instrument), SettingFormcs.Method_Type, new object[] { rebate_barrier });
                DataMaker.PriceRecorder(date, closingprice, instrument.Id, SimulatorClass.Environment.out_price, SimulatorClass.Environment.out_delta, SimulatorClass.Environment.out_gamma, SimulatorClass.Environment.out_theta, SimulatorClass.Environment.out_vega, SimulatorClass.Environment.out_rho);
                Price record_price = (from priceset in context.PriceSet where (priceset.Date == date && priceset.Instrument.Id == instrument.Id) select priceset).FirstOrDefault();
                TradeForm.update_trade(record_price, false);
            }
            catch
            {
            }
            refresh_view();
        }
        static public void refresh_view()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            priceList = new List<object>();
            var prices = from priceset in context.PriceSet orderby priceset.Id select new { ID = priceset.Id, Instrument = priceset.Instrument.Ticker, Date = priceset.Date, ClosingPrice = priceset.ClosingPrice, MarkPrice = priceset.MarkPrice, Delta = priceset.Delta, Gamma = priceset.Gamma, Theta = priceset.Theta, Vega = priceset.Vega, Rho = priceset.Rho };
            foreach (var price in prices)
            {
                priceList.Add(price);
            }
            try
            {
                dataGridViewHistPrice.DataSource = priceList;
                dataGridViewHistPrice.Refresh();
            }
            catch
            {
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            DataGridViewSelectedRowCollection selected_rows = dataGridViewHistPrice.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["ID"].Value);
                Price price = (from prices in context.PriceSet where prices.Id == id select prices).FirstOrDefault();
                try
                {
                    context.PriceSet.Remove(price);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
            refresh_view();
        }
    }
}
