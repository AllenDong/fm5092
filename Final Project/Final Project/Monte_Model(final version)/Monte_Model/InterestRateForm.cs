﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class InterestRateForm : Form
    {
        static public List<object> interestRatesList;
        public InterestRateForm()
        {
            InitializeComponent();
            refresh_view();
        }
        private void InterestRateForm_Load(object sender, EventArgs e)
        {
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                double interestrate = Convert.ToDouble(textBoxInterestRate.Text);
                double tenor = Convert.ToDouble(textBoxTenor.Text);
                DataMaker.InterestRateRecorder(tenor,interestrate);
            }
            catch
            {
            }
            refresh_view();
        }
        static public void refresh_view()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            interestRatesList = new List<object>();
            var rates = from interestrates in context.InterestRateSet orderby interestrates.Id select new { ID = interestrates.Id, Tenor = interestrates.Tenor, Rate = interestrates.Rate };
            foreach (var rate in rates)
            {
                interestRatesList.Add(rate);
            }
            try
            {
                dataGridViewInterestRate.DataSource = interestRatesList;
                dataGridViewInterestRate.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch
            {
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            DataGridViewSelectedRowCollection selected_rows = dataGridViewInterestRate.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["ID"].Value);
                InterestRate interestRate = (from interests in context.InterestRateSet where interests.Id == id select interests).FirstOrDefault();
                try
                {
                    context.InterestRateSet.Remove(interestRate);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
            refresh_view();
        }
    }
}
