﻿namespace Monte_Model
{
    partial class InstrumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelInstType = new System.Windows.Forms.Label();
            this.labelTicker = new System.Windows.Forms.Label();
            this.labelExchange = new System.Windows.Forms.Label();
            this.labelUnderlying = new System.Windows.Forms.Label();
            this.labelStrike = new System.Windows.Forms.Label();
            this.labelRebate = new System.Windows.Forms.Label();
            this.labelBarrier = new System.Windows.Forms.Label();
            this.labelBarrierType = new System.Windows.Forms.Label();
            this.labelCompany = new System.Windows.Forms.Label();
            this.labelCP = new System.Windows.Forms.Label();
            this.labelTenor = new System.Windows.Forms.Label();
            comboBoxUnderlying = new System.Windows.Forms.ComboBox();
            this.comboBoxBarrierType = new System.Windows.Forms.ComboBox();
            this.comboBoxInstType = new System.Windows.Forms.ComboBox();
            this.comboBoxCP = new System.Windows.Forms.ComboBox();
            this.textBoxStrike = new System.Windows.Forms.TextBox();
            this.textBoxCompanyName = new System.Windows.Forms.TextBox();
            this.textBoxExchange = new System.Windows.Forms.TextBox();
            this.textBoxTicker = new System.Windows.Forms.TextBox();
            this.textBoxBarrier = new System.Windows.Forms.TextBox();
            this.textBoxRebate = new System.Windows.Forms.TextBox();
            dataGridViewInstrument = new System.Windows.Forms.DataGridView();
            this.instrumentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelTimeStamp = new System.Windows.Forms.Label();
            this.dateTimePickerTimeStamp = new System.Windows.Forms.DateTimePicker();
            this.buttonSaveInst = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelEDPTip = new System.Windows.Forms.Label();
            this.textBoxTenor = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInstrument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelInstType
            // 
            this.labelInstType.AutoSize = true;
            this.labelInstType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstType.Location = new System.Drawing.Point(20, 35);
            this.labelInstType.Name = "labelInstType";
            this.labelInstType.Size = new System.Drawing.Size(153, 25);
            this.labelInstType.TabIndex = 0;
            this.labelInstType.Text = "Instrument Type";
            // 
            // labelTicker
            // 
            this.labelTicker.AutoSize = true;
            this.labelTicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTicker.Location = new System.Drawing.Point(20, 172);
            this.labelTicker.Name = "labelTicker";
            this.labelTicker.Size = new System.Drawing.Size(66, 25);
            this.labelTicker.TabIndex = 1;
            this.labelTicker.Text = "Ticker";
            // 
            // labelExchange
            // 
            this.labelExchange.AutoSize = true;
            this.labelExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExchange.Location = new System.Drawing.Point(20, 125);
            this.labelExchange.Name = "labelExchange";
            this.labelExchange.Size = new System.Drawing.Size(100, 25);
            this.labelExchange.TabIndex = 2;
            this.labelExchange.Text = "Exchange";
            // 
            // labelUnderlying
            // 
            this.labelUnderlying.AutoSize = true;
            this.labelUnderlying.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnderlying.Location = new System.Drawing.Point(20, 215);
            this.labelUnderlying.Name = "labelUnderlying";
            this.labelUnderlying.Size = new System.Drawing.Size(105, 25);
            this.labelUnderlying.TabIndex = 3;
            this.labelUnderlying.Text = "Underlying";
            // 
            // labelStrike
            // 
            this.labelStrike.AutoSize = true;
            this.labelStrike.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStrike.Location = new System.Drawing.Point(20, 350);
            this.labelStrike.Name = "labelStrike";
            this.labelStrike.Size = new System.Drawing.Size(62, 25);
            this.labelStrike.TabIndex = 4;
            this.labelStrike.Text = "Strike";
            // 
            // labelRebate
            // 
            this.labelRebate.AutoSize = true;
            this.labelRebate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRebate.Location = new System.Drawing.Point(20, 485);
            this.labelRebate.Name = "labelRebate";
            this.labelRebate.Size = new System.Drawing.Size(74, 25);
            this.labelRebate.TabIndex = 5;
            this.labelRebate.Text = "Rebate";
            // 
            // labelBarrier
            // 
            this.labelBarrier.AutoSize = true;
            this.labelBarrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBarrier.Location = new System.Drawing.Point(20, 440);
            this.labelBarrier.Name = "labelBarrier";
            this.labelBarrier.Size = new System.Drawing.Size(69, 25);
            this.labelBarrier.TabIndex = 6;
            this.labelBarrier.Text = "Barrier";
            // 
            // labelBarrierType
            // 
            this.labelBarrierType.AutoSize = true;
            this.labelBarrierType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBarrierType.Location = new System.Drawing.Point(20, 395);
            this.labelBarrierType.Name = "labelBarrierType";
            this.labelBarrierType.Size = new System.Drawing.Size(119, 25);
            this.labelBarrierType.TabIndex = 7;
            this.labelBarrierType.Text = "Barrier Type";
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompany.Location = new System.Drawing.Point(20, 80);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(154, 25);
            this.labelCompany.TabIndex = 8;
            this.labelCompany.Text = "Company Name";
            // 
            // labelCP
            // 
            this.labelCP.AutoSize = true;
            this.labelCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCP.Location = new System.Drawing.Point(20, 305);
            this.labelCP.Name = "labelCP";
            this.labelCP.Size = new System.Drawing.Size(91, 25);
            this.labelCP.TabIndex = 9;
            this.labelCP.Text = "Call / Put";
            // 
            // labelTenor
            // 
            this.labelTenor.AutoSize = true;
            this.labelTenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTenor.Location = new System.Drawing.Point(20, 260);
            this.labelTenor.Name = "labelTenor";
            this.labelTenor.Size = new System.Drawing.Size(64, 25);
            this.labelTenor.TabIndex = 10;
            this.labelTenor.Text = "Tenor";
            // 
            // comboBoxUnderlying
            // 
            comboBoxUnderlying.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            comboBoxUnderlying.FormattingEnabled = true;
            comboBoxUnderlying.Location = new System.Drawing.Point(248, 207);
            comboBoxUnderlying.Name = "comboBoxUnderlying";
            comboBoxUnderlying.Size = new System.Drawing.Size(186, 33);
            comboBoxUnderlying.TabIndex = 11;
            // 
            // comboBoxBarrierType
            // 
            this.comboBoxBarrierType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxBarrierType.FormattingEnabled = true;
            this.comboBoxBarrierType.Items.AddRange(new object[] {
            "UpIn",
            "UpOut",
            "DownIn",
            "DownOut"});
            this.comboBoxBarrierType.Location = new System.Drawing.Point(248, 387);
            this.comboBoxBarrierType.Name = "comboBoxBarrierType";
            this.comboBoxBarrierType.Size = new System.Drawing.Size(186, 33);
            this.comboBoxBarrierType.TabIndex = 12;
            // 
            // comboBoxInstType
            // 
            this.comboBoxInstType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxInstType.FormattingEnabled = true;
            this.comboBoxInstType.Location = new System.Drawing.Point(248, 27);
            this.comboBoxInstType.Name = "comboBoxInstType";
            this.comboBoxInstType.Size = new System.Drawing.Size(186, 33);
            this.comboBoxInstType.TabIndex = 13;
            this.comboBoxInstType.SelectedValueChanged += new System.EventHandler(this.comboBoxInstType_SelectedIndexChanged);
            // 
            // comboBoxCP
            // 
            this.comboBoxCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCP.FormattingEnabled = true;
            this.comboBoxCP.Items.AddRange(new object[] {
            "Call",
            "Put"});
            this.comboBoxCP.Location = new System.Drawing.Point(248, 297);
            this.comboBoxCP.Name = "comboBoxCP";
            this.comboBoxCP.Size = new System.Drawing.Size(186, 33);
            this.comboBoxCP.TabIndex = 14;
            // 
            // textBoxStrike
            // 
            this.textBoxStrike.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStrike.Location = new System.Drawing.Point(248, 345);
            this.textBoxStrike.Name = "textBoxStrike";
            this.textBoxStrike.Size = new System.Drawing.Size(186, 30);
            this.textBoxStrike.TabIndex = 16;
            // 
            // textBoxCompanyName
            // 
            this.textBoxCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCompanyName.Location = new System.Drawing.Point(248, 75);
            this.textBoxCompanyName.Name = "textBoxCompanyName";
            this.textBoxCompanyName.Size = new System.Drawing.Size(186, 30);
            this.textBoxCompanyName.TabIndex = 17;
            // 
            // textBoxExchange
            // 
            this.textBoxExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxExchange.Location = new System.Drawing.Point(248, 120);
            this.textBoxExchange.Name = "textBoxExchange";
            this.textBoxExchange.Size = new System.Drawing.Size(186, 30);
            this.textBoxExchange.TabIndex = 18;
            // 
            // textBoxTicker
            // 
            this.textBoxTicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTicker.Location = new System.Drawing.Point(248, 167);
            this.textBoxTicker.Name = "textBoxTicker";
            this.textBoxTicker.Size = new System.Drawing.Size(186, 30);
            this.textBoxTicker.TabIndex = 19;
            // 
            // textBoxBarrier
            // 
            this.textBoxBarrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBarrier.Location = new System.Drawing.Point(248, 435);
            this.textBoxBarrier.Name = "textBoxBarrier";
            this.textBoxBarrier.Size = new System.Drawing.Size(186, 30);
            this.textBoxBarrier.TabIndex = 20;
            // 
            // textBoxRebate
            // 
            this.textBoxRebate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRebate.Location = new System.Drawing.Point(248, 480);
            this.textBoxRebate.Name = "textBoxRebate";
            this.textBoxRebate.Size = new System.Drawing.Size(186, 30);
            this.textBoxRebate.TabIndex = 21;
            // 
            // dataGridViewInstrument
            // 
            dataGridViewInstrument.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewInstrument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewInstrument.Location = new System.Drawing.Point(503, 55);
            dataGridViewInstrument.Name = "dataGridViewInstrument";
            dataGridViewInstrument.RowHeadersVisible = false;
            dataGridViewInstrument.RowTemplate.Height = 28;
            dataGridViewInstrument.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewInstrument.Size = new System.Drawing.Size(750, 505);
            dataGridViewInstrument.TabIndex = 22;
            // 
            // instrumentBindingSource
            // 
            this.instrumentBindingSource.DataSource = typeof(Monte_Model.Instrument);
            // 
            // labelTimeStamp
            // 
            this.labelTimeStamp.AutoSize = true;
            this.labelTimeStamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeStamp.Location = new System.Drawing.Point(20, 530);
            this.labelTimeStamp.Name = "labelTimeStamp";
            this.labelTimeStamp.Size = new System.Drawing.Size(118, 25);
            this.labelTimeStamp.TabIndex = 23;
            this.labelTimeStamp.Text = "Time Stamp";
            // 
            // dateTimePickerTimeStamp
            // 
            this.dateTimePickerTimeStamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerTimeStamp.Location = new System.Drawing.Point(248, 530);
            this.dateTimePickerTimeStamp.Name = "dateTimePickerTimeStamp";
            this.dateTimePickerTimeStamp.Size = new System.Drawing.Size(186, 30);
            this.dateTimePickerTimeStamp.TabIndex = 24;
            // 
            // buttonSaveInst
            // 
            this.buttonSaveInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveInst.Location = new System.Drawing.Point(25, 575);
            this.buttonSaveInst.Name = "buttonSaveInst";
            this.buttonSaveInst.Size = new System.Drawing.Size(409, 37);
            this.buttonSaveInst.TabIndex = 25;
            this.buttonSaveInst.Text = "SAVE INSTRUMENT";
            this.buttonSaveInst.UseVisualStyleBackColor = true;
            this.buttonSaveInst.Click += new System.EventHandler(this.buttonSaveInst_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(1049, 575);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(146, 37);
            this.buttonRefresh.TabIndex = 26;
            this.buttonRefresh.Text = "DELETE";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // labelEDPTip
            // 
            this.labelEDPTip.AutoSize = true;
            this.labelEDPTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEDPTip.Location = new System.Drawing.Point(498, 23);
            this.labelEDPTip.Name = "labelEDPTip";
            this.labelEDPTip.Size = new System.Drawing.Size(231, 25);
            this.labelEDPTip.TabIndex = 27;
            this.labelEDPTip.Text = "Edit/Delete Price in Table";
            // 
            // textBoxTenor
            // 
            this.textBoxTenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTenor.Location = new System.Drawing.Point(248, 255);
            this.textBoxTenor.Name = "textBoxTenor";
            this.textBoxTenor.Size = new System.Drawing.Size(186, 30);
            this.textBoxTenor.TabIndex = 28;
            // 
            // InstrumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1399, 627);
            this.Controls.Add(this.textBoxTenor);
            this.Controls.Add(this.labelEDPTip);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonSaveInst);
            this.Controls.Add(this.dateTimePickerTimeStamp);
            this.Controls.Add(this.labelTimeStamp);
            this.Controls.Add(dataGridViewInstrument);
            this.Controls.Add(this.textBoxRebate);
            this.Controls.Add(this.textBoxBarrier);
            this.Controls.Add(this.textBoxTicker);
            this.Controls.Add(this.textBoxExchange);
            this.Controls.Add(this.textBoxCompanyName);
            this.Controls.Add(this.textBoxStrike);
            this.Controls.Add(this.comboBoxCP);
            this.Controls.Add(this.comboBoxInstType);
            this.Controls.Add(this.comboBoxBarrierType);
            this.Controls.Add(comboBoxUnderlying);
            this.Controls.Add(this.labelTenor);
            this.Controls.Add(this.labelCP);
            this.Controls.Add(this.labelCompany);
            this.Controls.Add(this.labelBarrierType);
            this.Controls.Add(this.labelBarrier);
            this.Controls.Add(this.labelRebate);
            this.Controls.Add(this.labelStrike);
            this.Controls.Add(this.labelUnderlying);
            this.Controls.Add(this.labelExchange);
            this.Controls.Add(this.labelTicker);
            this.Controls.Add(this.labelInstType);
            this.Name = "InstrumentForm";
            this.Text = "InstrumentForm";
            this.Load += new System.EventHandler(this.InstrumentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInstrument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInstType;
        private System.Windows.Forms.Label labelTicker;
        private System.Windows.Forms.Label labelExchange;
        private System.Windows.Forms.Label labelUnderlying;
        private System.Windows.Forms.Label labelStrike;
        private System.Windows.Forms.Label labelRebate;
        private System.Windows.Forms.Label labelBarrier;
        private System.Windows.Forms.Label labelBarrierType;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.Label labelCP;
        private System.Windows.Forms.Label labelTenor;
        static private System.Windows.Forms.ComboBox comboBoxUnderlying = new System.Windows.Forms.ComboBox();
        private System.Windows.Forms.ComboBox comboBoxBarrierType;
        private System.Windows.Forms.ComboBox comboBoxInstType;
        private System.Windows.Forms.ComboBox comboBoxCP;
        private System.Windows.Forms.TextBox textBoxStrike;
        private System.Windows.Forms.TextBox textBoxCompanyName;
        private System.Windows.Forms.TextBox textBoxExchange;
        private System.Windows.Forms.TextBox textBoxTicker;
        private System.Windows.Forms.TextBox textBoxBarrier;
        private System.Windows.Forms.TextBox textBoxRebate;
        private System.Windows.Forms.Label labelTimeStamp;
        private System.Windows.Forms.DateTimePicker dateTimePickerTimeStamp;
        private System.Windows.Forms.Button buttonSaveInst;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Label labelEDPTip;
        private System.Windows.Forms.BindingSource instrumentBindingSource;
        private System.Windows.Forms.TextBox textBoxTenor;
        static private System.Windows.Forms.DataGridView dataGridViewInstrument = new System.Windows.Forms.DataGridView();
    }
}