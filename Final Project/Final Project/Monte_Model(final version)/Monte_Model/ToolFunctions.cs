﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_Model
{
    class ToolFunctions
    {
        static public SimulatorClass.Option_Type InstrumentType(Instrument instrument)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            switch ((from instname in context.InstTypeSet where instname.Id == instrument.InstTypeId select instname).FirstOrDefault().TypeName)
            {
                case "European":
                    return SimulatorClass.Option_Type.European;
                case "Digital":
                    return SimulatorClass.Option_Type.Digital;
                case "Range":
                    return SimulatorClass.Option_Type.Range;
                case "Asian":
                    return SimulatorClass.Option_Type.Asian;
                case "Lookback":
                    return SimulatorClass.Option_Type.Lookback;
                case "Barrier":
                    return SimulatorClass.Option_Type.Barrier;
                default:
                    return SimulatorClass.Option_Type.Stock;
            }
        }
        static public SimulatorClass.Cal_Type[] option_Type(Instrument instrument)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            SimulatorClass.Cal_Type[] cal_Type;
            if (instrument.IsCall == null)
            {
                cal_Type = new SimulatorClass.Cal_Type[1];
            }
            else if (instrument.InstTypeId == (from instTypes in context.InstTypeSet where instTypes.TypeName == "Barrier" select instTypes.Id).FirstOrDefault())
            {
                SimulatorClass.Cal_Type barrierType = new SimulatorClass.Cal_Type();
                HistoricalPriceForm.rebate_barrier = Convert.ToDouble(instrument.Barrier);
                switch (instrument.BarrierType)
                {
                    case "UpIn":
                        barrierType = SimulatorClass.Cal_Type.Up_In;
                        break;
                    case "UpOut":
                        barrierType = SimulatorClass.Cal_Type.Up_Out;
                        break;
                    case "DownIn":
                        barrierType = SimulatorClass.Cal_Type.Down_In;
                        break;
                    case "DownOut":
                        barrierType = SimulatorClass.Cal_Type.Down_Out;
                        break;
                }
                cal_Type = new SimulatorClass.Cal_Type[] { (Convert.ToBoolean(instrument.IsCall) ? SimulatorClass.Cal_Type.Call : SimulatorClass.Cal_Type.Put), barrierType };
            }
            else
            {
                HistoricalPriceForm.rebate_barrier = Convert.ToDouble(instrument.Rebate == null ? 0 : instrument.Rebate);
                cal_Type = new SimulatorClass.Cal_Type[] { (Convert.ToBoolean(instrument.IsCall) ? SimulatorClass.Cal_Type.Call : SimulatorClass.Cal_Type.Put) };
            }
            return cal_Type;
        }
        static public double generate_interest_rate(double tenor)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            var ratelist = (from rates in context.InterestRateSet orderby Math.Abs(rates.Tenor - tenor) select rates).ToArray();
            return (ratelist[1].Rate - ratelist[0].Rate) / (ratelist[1].Tenor - ratelist[0].Tenor) * (tenor - ratelist[0].Tenor) + ratelist[0].Rate;
        }
    }
}
