﻿namespace Monte_Model
{
    partial class InterestRateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxInterestRate = new System.Windows.Forms.TextBox();
            this.labelInterestRate = new System.Windows.Forms.Label();
            dataGridViewInterestRate = new System.Windows.Forms.DataGridView();
            this.textBoxTenor = new System.Windows.Forms.TextBox();
            this.labelTenor = new System.Windows.Forms.Label();
            this.interestRateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInterestRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRateBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(368, 20);
            this.label1.TabIndex = 36;
            this.label1.Text = "A new rate with same tenor will cover the older one!";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.Location = new System.Drawing.Point(349, 64);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(112, 30);
            this.buttonAdd.TabIndex = 35;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxInterestRate
            // 
            this.textBoxInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInterestRate.Location = new System.Drawing.Point(141, 19);
            this.textBoxInterestRate.Name = "textBoxInterestRate";
            this.textBoxInterestRate.Size = new System.Drawing.Size(186, 26);
            this.textBoxInterestRate.TabIndex = 34;
            // 
            // labelInterestRate
            // 
            this.labelInterestRate.AutoSize = true;
            this.labelInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInterestRate.Location = new System.Drawing.Point(21, 22);
            this.labelInterestRate.Name = "labelInterestRate";
            this.labelInterestRate.Size = new System.Drawing.Size(103, 20);
            this.labelInterestRate.TabIndex = 33;
            this.labelInterestRate.Text = "Interest Rate";
            // 
            // dataGridViewInterestRate
            // 
            dataGridViewInterestRate.AllowUserToOrderColumns = true;
            dataGridViewInterestRate.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewInterestRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewInterestRate.GridColor = System.Drawing.SystemColors.Control;
            dataGridViewInterestRate.Location = new System.Drawing.Point(26, 134);
            dataGridViewInterestRate.Name = "dataGridViewInterestRate";
            dataGridViewInterestRate.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewInterestRate.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewInterestRate.RowHeadersVisible = false;
            dataGridViewInterestRate.RowTemplate.Height = 28;
            dataGridViewInterestRate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewInterestRate.Size = new System.Drawing.Size(435, 216);
            dataGridViewInterestRate.TabIndex = 32;
            // 
            // textBoxTenor
            // 
            this.textBoxTenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTenor.Location = new System.Drawing.Point(141, 64);
            this.textBoxTenor.Name = "textBoxTenor";
            this.textBoxTenor.Size = new System.Drawing.Size(186, 26);
            this.textBoxTenor.TabIndex = 38;
            // 
            // labelTenor
            // 
            this.labelTenor.AutoSize = true;
            this.labelTenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTenor.Location = new System.Drawing.Point(21, 67);
            this.labelTenor.Name = "labelTenor";
            this.labelTenor.Size = new System.Drawing.Size(50, 20);
            this.labelTenor.TabIndex = 37;
            this.labelTenor.Text = "Tenor";
            // 
            // interestRateBindingSource
            // 
            this.interestRateBindingSource.DataSource = typeof(Monte_Model.InterestRate);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.Location = new System.Drawing.Point(349, 365);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(112, 30);
            this.buttonDelete.TabIndex = 39;
            this.buttonDelete.Text = "DELETE";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // InterestRateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 407);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.textBoxTenor);
            this.Controls.Add(this.labelTenor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxInterestRate);
            this.Controls.Add(this.labelInterestRate);
            this.Controls.Add(dataGridViewInterestRate);
            this.Name = "InterestRateForm";
            this.Text = "InterestRateForm";
            this.Load += new System.EventHandler(this.InterestRateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(dataGridViewInterestRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRateBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textBoxInterestRate;
        private System.Windows.Forms.Label labelInterestRate;
        private System.Windows.Forms.TextBox textBoxTenor;
        private System.Windows.Forms.Label labelTenor;
        private System.Windows.Forms.BindingSource interestRateBindingSource;
        private System.Windows.Forms.Button buttonDelete;
        static private System.Windows.Forms.DataGridView dataGridViewInterestRate = new System.Windows.Forms.DataGridView();
    }
}