﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_Model
{
    public partial class PriceAnalysisForm : Form
    {
        static public List<object> priceList = new List<object>();
        public PriceAnalysisForm()
        {
            InitializeComponent();
            updateCombo();
        }
        public void Refresh(Instrument instrument)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            priceList = new List<object>();
            var prices = from priceset in context.PriceSet where priceset.Instrument.Id == instrument.Id orderby priceset.Date ascending select new { ID = priceset.Id, Date = priceset.Date, Instrument = priceset.Instrument.Ticker, ClosingPrice = priceset.ClosingPrice, MarkPrice = priceset.MarkPrice, Delta = priceset.Delta, Gamma = priceset.Gamma, Theta = priceset.Theta, Vega = priceset.Vega, Rho = priceset.Rho };
            foreach (var price in prices)
            {
                priceList.Add(price);
            }
            dataGridViewPrice.DataSource = priceList;
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            try
            {
                Instrument instrument = (from instrumentset in context.InstrumentSet where instrumentset.Ticker == comboBoxInstrument.SelectedItem.ToString() select instrumentset).FirstOrDefault();
                Refresh(instrument);
            }
            catch
            {
            }
        }
        private void updateCombo()
        {
            Monte_ModelEntities context = new Monte_ModelEntities();
            foreach (Instrument instrument in context.InstrumentSet)
            {
                comboBoxInstrument.Items.Add(instrument.Ticker);
            }
        }
    }
}
