﻿namespace Monte_Model
{
    partial class HistoricalPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InstNameList = new System.Windows.Forms.ListBox();
            this.HistoryDate = new System.Windows.Forms.DateTimePicker();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.textBoxClosingPrice = new System.Windows.Forms.TextBox();
            this.buttonADD = new System.Windows.Forms.Button();
            this.labelEDPTip = new System.Windows.Forms.Label();
            dataGridViewHistPrice = new System.Windows.Forms.DataGridView();
            this.priceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonDelete = new System.Windows.Forms.Button();
            this.labelWarn = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(dataGridViewHistPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // InstNameList
            // 
            this.InstNameList.BackColor = System.Drawing.SystemColors.Menu;
            this.InstNameList.Font = new System.Drawing.Font("Microsoft YaHei UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InstNameList.FormattingEnabled = true;
            this.InstNameList.ItemHeight = 21;
            this.InstNameList.Location = new System.Drawing.Point(16, 30);
            this.InstNameList.Margin = new System.Windows.Forms.Padding(4);
            this.InstNameList.Name = "InstNameList";
            this.InstNameList.Size = new System.Drawing.Size(248, 508);
            this.InstNameList.Sorted = true;
            this.InstNameList.TabIndex = 0;
            // 
            // HistoryDate
            // 
            this.HistoryDate.CalendarMonthBackground = System.Drawing.SystemColors.Menu;
            this.HistoryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HistoryDate.Location = new System.Drawing.Point(315, 65);
            this.HistoryDate.Margin = new System.Windows.Forms.Padding(4);
            this.HistoryDate.Name = "HistoryDate";
            this.HistoryDate.Size = new System.Drawing.Size(265, 26);
            this.HistoryDate.TabIndex = 1;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Location = new System.Drawing.Point(310, 30);
            this.labelDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(82, 20);
            this.labelDate.TabIndex = 2;
            this.labelDate.Text = "Date Time";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrice.Location = new System.Drawing.Point(652, 30);
            this.labelPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(100, 20);
            this.labelPrice.TabIndex = 3;
            this.labelPrice.Text = "Closing Price";
            // 
            // textBoxClosingPrice
            // 
            this.textBoxClosingPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClosingPrice.Location = new System.Drawing.Point(656, 65);
            this.textBoxClosingPrice.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClosingPrice.Name = "textBoxClosingPrice";
            this.textBoxClosingPrice.Size = new System.Drawing.Size(197, 26);
            this.textBoxClosingPrice.TabIndex = 4;
            // 
            // buttonADD
            // 
            this.buttonADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonADD.Location = new System.Drawing.Point(901, 65);
            this.buttonADD.Name = "buttonADD";
            this.buttonADD.Size = new System.Drawing.Size(81, 30);
            this.buttonADD.TabIndex = 5;
            this.buttonADD.Text = "ADD";
            this.buttonADD.UseVisualStyleBackColor = true;
            this.buttonADD.Click += new System.EventHandler(this.buttonADD_Click);
            // 
            // labelEDPTip
            // 
            this.labelEDPTip.AutoSize = true;
            this.labelEDPTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEDPTip.Location = new System.Drawing.Point(311, 132);
            this.labelEDPTip.Name = "labelEDPTip";
            this.labelEDPTip.Size = new System.Drawing.Size(186, 20);
            this.labelEDPTip.TabIndex = 6;
            this.labelEDPTip.Text = "Edit/Delete Price in Table";
            // 
            // dataGridViewHistPrice
            // 
            dataGridViewHistPrice.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewHistPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewHistPrice.Location = new System.Drawing.Point(315, 158);
            dataGridViewHistPrice.Name = "dataGridViewHistPrice";
            dataGridViewHistPrice.RowTemplate.Height = 28;
            dataGridViewHistPrice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridViewHistPrice.Size = new System.Drawing.Size(667, 331);
            dataGridViewHistPrice.TabIndex = 7;
            // 
            // priceBindingSource
            // 
            this.priceBindingSource.DataSource = typeof(Monte_Model.Price);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.Location = new System.Drawing.Point(753, 503);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(229, 35);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "DELETE";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // labelWarn
            // 
            this.labelWarn.AutoSize = true;
            this.labelWarn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWarn.Location = new System.Drawing.Point(311, 498);
            this.labelWarn.Name = "labelWarn";
            this.labelWarn.Size = new System.Drawing.Size(322, 40);
            this.labelWarn.TabIndex = 10;
            this.labelWarn.Text = "Notice if the price was used for trades, you \r\ncannot delete it without deleting " +
    "trades firstly";
            // 
            // HistoricalPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 555);
            this.Controls.Add(this.labelWarn);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(dataGridViewHistPrice);
            this.Controls.Add(this.labelEDPTip);
            this.Controls.Add(this.buttonADD);
            this.Controls.Add(this.textBoxClosingPrice);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.HistoryDate);
            this.Controls.Add(this.InstNameList);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HistoricalPriceForm";
            this.Text = "HistoricalPriceForm";
            this.Load += new System.EventHandler(this.HistoricalPriceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(dataGridViewHistPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox InstNameList;
        private System.Windows.Forms.DateTimePicker HistoryDate;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox textBoxClosingPrice;
        private System.Windows.Forms.Button buttonADD;
        private System.Windows.Forms.Label labelEDPTip;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.BindingSource priceBindingSource;
        private System.Windows.Forms.Label labelWarn;
        static private System.Windows.Forms.DataGridView dataGridViewHistPrice = new System.Windows.Forms.DataGridView();
    }
}