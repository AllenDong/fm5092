﻿namespace Monte_Model
{
    partial class InterestAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartInterestRate = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.textBoxTenor = new System.Windows.Forms.TextBox();
            this.textBoxRate = new System.Windows.Forms.TextBox();
            this.labelTenor = new System.Windows.Forms.Label();
            this.labelRate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartInterestRate)).BeginInit();
            this.SuspendLayout();
            // 
            // chartInterestRate
            // 
            this.chartInterestRate.BackColor = System.Drawing.Color.WhiteSmoke;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 94F;
            chartArea1.Position.Width = 94F;
            chartArea1.Position.X = 1F;
            chartArea1.Position.Y = 3F;
            this.chartInterestRate.ChartAreas.Add(chartArea1);
            this.chartInterestRate.Location = new System.Drawing.Point(12, 81);
            this.chartInterestRate.Name = "chartInterestRate";
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            series1.BorderWidth = 5;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.IsVisibleInLegend = false;
            series1.LabelBorderWidth = 5;
            series1.LegendText = "0";
            series1.Name = "SeriesInterestRate";
            series1.SmartLabelStyle.CalloutLineWidth = 5;
            this.chartInterestRate.Series.Add(series1);
            this.chartInterestRate.Size = new System.Drawing.Size(508, 244);
            this.chartInterestRate.TabIndex = 0;
            this.chartInterestRate.Text = "Interest Rate";
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(222, 39);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(116, 36);
            this.buttonSearch.TabIndex = 1;
            this.buttonSearch.Text = "SEARCH";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // textBoxTenor
            // 
            this.textBoxTenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTenor.Location = new System.Drawing.Point(73, 40);
            this.textBoxTenor.Name = "textBoxTenor";
            this.textBoxTenor.Size = new System.Drawing.Size(105, 30);
            this.textBoxTenor.TabIndex = 2;
            // 
            // textBoxRate
            // 
            this.textBoxRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRate.Location = new System.Drawing.Point(379, 40);
            this.textBoxRate.Name = "textBoxRate";
            this.textBoxRate.ReadOnly = true;
            this.textBoxRate.Size = new System.Drawing.Size(105, 30);
            this.textBoxRate.TabIndex = 3;
            // 
            // labelTenor
            // 
            this.labelTenor.AutoSize = true;
            this.labelTenor.Location = new System.Drawing.Point(97, 17);
            this.labelTenor.Name = "labelTenor";
            this.labelTenor.Size = new System.Drawing.Size(50, 20);
            this.labelTenor.TabIndex = 4;
            this.labelTenor.Text = "Tenor";
            // 
            // labelRate
            // 
            this.labelRate.AutoSize = true;
            this.labelRate.Location = new System.Drawing.Point(408, 17);
            this.labelRate.Name = "labelRate";
            this.labelRate.Size = new System.Drawing.Size(44, 20);
            this.labelRate.TabIndex = 5;
            this.labelRate.Text = "Rate";
            // 
            // InterestAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 336);
            this.Controls.Add(this.labelRate);
            this.Controls.Add(this.labelTenor);
            this.Controls.Add(this.textBoxRate);
            this.Controls.Add(this.textBoxTenor);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.chartInterestRate);
            this.Name = "InterestAnalysis";
            this.Text = "InterestAnalysis";
            this.Load += new System.EventHandler(this.InterestAnalysis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartInterestRate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartInterestRate;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.TextBox textBoxTenor;
        private System.Windows.Forms.TextBox textBoxRate;
        private System.Windows.Forms.Label labelTenor;
        private System.Windows.Forms.Label labelRate;
    }
}