﻿namespace Monte_test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SimulatorStart = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.operationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startSimulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.europeanOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rangeOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asianOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barrierOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lookBackOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.digitalOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userGuideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CommingSoon = new System.Windows.Forms.Label();
            this.OptionBox = new System.Windows.Forms.GroupBox();
            this.TypeBox3 = new System.Windows.Forms.GroupBox();
            this.call = new System.Windows.Forms.RadioButton();
            this.put = new System.Windows.Forms.RadioButton();
            this.TypeBox = new System.Windows.Forms.GroupBox();
            this.threadAmt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.multi = new System.Windows.Forms.DomainUpDown();
            this.cv = new System.Windows.Forms.DomainUpDown();
            this.anti = new System.Windows.Forms.DomainUpDown();
            this.frequency = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.volatility = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tenor = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.strikrPrice = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.spotPrice = new System.Windows.Forms.TextBox();
            this.rebate_barrier_label = new System.Windows.Forms.Label();
            this.interestRate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.trails = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.rebate_barrier = new System.Windows.Forms.TextBox();
            this.WarnTxt = new System.Windows.Forms.Label();
            this.BarrierType = new System.Windows.Forms.DomainUpDown();
            this.menuStrip.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.OptionBox.SuspendLayout();
            this.TypeBox3.SuspendLayout();
            this.TypeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SimulatorStart
            // 
            this.SimulatorStart.Location = new System.Drawing.Point(430, 368);
            this.SimulatorStart.Name = "SimulatorStart";
            this.SimulatorStart.Size = new System.Drawing.Size(147, 37);
            this.SimulatorStart.TabIndex = 4;
            this.SimulatorStart.Text = "Start Simulator";
            this.SimulatorStart.UseVisualStyleBackColor = true;
            this.SimulatorStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operationToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(603, 32);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "UseIt";
            // 
            // operationToolStripMenuItem
            // 
            this.operationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startSimulatorToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.operationToolStripMenuItem.Name = "operationToolStripMenuItem";
            this.operationToolStripMenuItem.Size = new System.Drawing.Size(110, 28);
            this.operationToolStripMenuItem.Text = "Operation";
            // 
            // startSimulatorToolStripMenuItem
            // 
            this.startSimulatorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.europeanOptionToolStripMenuItem,
            this.rangeOptionToolStripMenuItem,
            this.asianOptionToolStripMenuItem,
            this.barrierOptionToolStripMenuItem,
            this.lookBackOptionToolStripMenuItem,
            this.digitalOptionToolStripMenuItem});
            this.startSimulatorToolStripMenuItem.Name = "startSimulatorToolStripMenuItem";
            this.startSimulatorToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.startSimulatorToolStripMenuItem.Text = "Start Simulator";
            // 
            // europeanOptionToolStripMenuItem
            // 
            this.europeanOptionToolStripMenuItem.Name = "europeanOptionToolStripMenuItem";
            this.europeanOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.europeanOptionToolStripMenuItem.Text = "European Option";
            this.europeanOptionToolStripMenuItem.Click += new System.EventHandler(this.europeanOptionToolStripMenuItem_Click);
            // 
            // rangeOptionToolStripMenuItem
            // 
            this.rangeOptionToolStripMenuItem.Name = "rangeOptionToolStripMenuItem";
            this.rangeOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.rangeOptionToolStripMenuItem.Text = "Range Option";
            this.rangeOptionToolStripMenuItem.Click += new System.EventHandler(this.rangeOptionToolStripMenuItem_Click);
            // 
            // asianOptionToolStripMenuItem
            // 
            this.asianOptionToolStripMenuItem.Name = "asianOptionToolStripMenuItem";
            this.asianOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.asianOptionToolStripMenuItem.Text = "Asian Option";
            this.asianOptionToolStripMenuItem.Click += new System.EventHandler(this.asianOptionToolStripMenuItem_Click);
            // 
            // barrierOptionToolStripMenuItem
            // 
            this.barrierOptionToolStripMenuItem.Name = "barrierOptionToolStripMenuItem";
            this.barrierOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.barrierOptionToolStripMenuItem.Text = "Barrier Option";
            this.barrierOptionToolStripMenuItem.Click += new System.EventHandler(this.barrierOptionToolStripMenuItem_Click);
            // 
            // lookBackOptionToolStripMenuItem
            // 
            this.lookBackOptionToolStripMenuItem.Name = "lookBackOptionToolStripMenuItem";
            this.lookBackOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.lookBackOptionToolStripMenuItem.Text = "LookBack Option";
            this.lookBackOptionToolStripMenuItem.Click += new System.EventHandler(this.lookBackOptionToolStripMenuItem_Click);
            // 
            // digitalOptionToolStripMenuItem
            // 
            this.digitalOptionToolStripMenuItem.Name = "digitalOptionToolStripMenuItem";
            this.digitalOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.digitalOptionToolStripMenuItem.Text = "Digital Option";
            this.digitalOptionToolStripMenuItem.Click += new System.EventHandler(this.digitalOptionToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userGuideToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(63, 28);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // userGuideToolStripMenuItem
            // 
            this.userGuideToolStripMenuItem.Name = "userGuideToolStripMenuItem";
            this.userGuideToolStripMenuItem.Size = new System.Drawing.Size(186, 30);
            this.userGuideToolStripMenuItem.Text = "User Guide";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CommingSoon);
            this.groupBox3.Location = new System.Drawing.Point(27, 48);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(550, 305);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Warning";
            // 
            // CommingSoon
            // 
            this.CommingSoon.AutoSize = true;
            this.CommingSoon.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CommingSoon.Location = new System.Drawing.Point(57, 103);
            this.CommingSoon.Name = "CommingSoon";
            this.CommingSoon.Size = new System.Drawing.Size(458, 69);
            this.CommingSoon.TabIndex = 7;
            this.CommingSoon.Text = "Comming Soon!";
            // 
            // OptionBox
            // 
            this.OptionBox.Controls.Add(this.TypeBox3);
            this.OptionBox.Controls.Add(this.TypeBox);
            this.OptionBox.Controls.Add(this.frequency);
            this.OptionBox.Controls.Add(this.label26);
            this.OptionBox.Controls.Add(this.label25);
            this.OptionBox.Controls.Add(this.label24);
            this.OptionBox.Controls.Add(this.volatility);
            this.OptionBox.Controls.Add(this.label23);
            this.OptionBox.Controls.Add(this.tenor);
            this.OptionBox.Controls.Add(this.label22);
            this.OptionBox.Controls.Add(this.strikrPrice);
            this.OptionBox.Controls.Add(this.label21);
            this.OptionBox.Controls.Add(this.spotPrice);
            this.OptionBox.Controls.Add(this.rebate_barrier_label);
            this.OptionBox.Controls.Add(this.interestRate);
            this.OptionBox.Controls.Add(this.label19);
            this.OptionBox.Controls.Add(this.trails);
            this.OptionBox.Controls.Add(this.label18);
            this.OptionBox.Controls.Add(this.rebate_barrier);
            this.OptionBox.Location = new System.Drawing.Point(27, 48);
            this.OptionBox.Name = "OptionBox";
            this.OptionBox.Size = new System.Drawing.Size(550, 305);
            this.OptionBox.TabIndex = 18;
            this.OptionBox.TabStop = false;
            this.OptionBox.Text = "Option";
            // 
            // TypeBox3
            // 
            this.TypeBox3.Controls.Add(this.BarrierType);
            this.TypeBox3.Controls.Add(this.call);
            this.TypeBox3.Controls.Add(this.put);
            this.TypeBox3.Location = new System.Drawing.Point(344, 0);
            this.TypeBox3.Name = "TypeBox3";
            this.TypeBox3.Size = new System.Drawing.Size(206, 138);
            this.TypeBox3.TabIndex = 19;
            this.TypeBox3.TabStop = false;
            this.TypeBox3.Text = "Type";
            // 
            // call
            // 
            this.call.AutoSize = true;
            this.call.Checked = true;
            this.call.Location = new System.Drawing.Point(45, 32);
            this.call.Name = "call";
            this.call.Size = new System.Drawing.Size(111, 24);
            this.call.TabIndex = 0;
            this.call.TabStop = true;
            this.call.Text = "Call Option";
            this.call.UseVisualStyleBackColor = true;
            // 
            // put
            // 
            this.put.AutoSize = true;
            this.put.Location = new System.Drawing.Point(45, 69);
            this.put.Name = "put";
            this.put.Size = new System.Drawing.Size(109, 24);
            this.put.TabIndex = 1;
            this.put.TabStop = true;
            this.put.Text = "Put Option";
            this.put.UseVisualStyleBackColor = true;
            // 
            // TypeBox
            // 
            this.TypeBox.Controls.Add(this.threadAmt);
            this.TypeBox.Controls.Add(this.label20);
            this.TypeBox.Controls.Add(this.multi);
            this.TypeBox.Controls.Add(this.cv);
            this.TypeBox.Controls.Add(this.anti);
            this.TypeBox.Location = new System.Drawing.Point(344, 0);
            this.TypeBox.Name = "TypeBox";
            this.TypeBox.Size = new System.Drawing.Size(206, 305);
            this.TypeBox.TabIndex = 13;
            this.TypeBox.TabStop = false;
            // 
            // threadAmt
            // 
            this.threadAmt.Location = new System.Drawing.Point(143, 263);
            this.threadAmt.Name = "threadAmt";
            this.threadAmt.Size = new System.Drawing.Size(41, 26);
            this.threadAmt.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft YaHei UI", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(29, 268);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(118, 17);
            this.label20.TabIndex = 19;
            this.label20.Text = "Threads Amount :";
            // 
            // multi
            // 
            this.multi.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.multi.Items.Add("Yes");
            this.multi.Items.Add("No");
            this.multi.Location = new System.Drawing.Point(38, 226);
            this.multi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.multi.Name = "multi";
            this.multi.ReadOnly = true;
            this.multi.Size = new System.Drawing.Size(146, 26);
            this.multi.TabIndex = 4;
            this.multi.Text = "Multi-Thread?";
            this.multi.SelectedItemChanged += new System.EventHandler(this.multi_SelectedItemChanged);
            // 
            // cv
            // 
            this.cv.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cv.Items.Add("Yes");
            this.cv.Items.Add("No");
            this.cv.Location = new System.Drawing.Point(38, 191);
            this.cv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cv.Name = "cv";
            this.cv.ReadOnly = true;
            this.cv.Size = new System.Drawing.Size(146, 26);
            this.cv.TabIndex = 3;
            this.cv.Text = "CV-Method?";
            // 
            // anti
            // 
            this.anti.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.anti.Items.Add("Yes");
            this.anti.Items.Add("No");
            this.anti.Location = new System.Drawing.Point(38, 155);
            this.anti.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.anti.Name = "anti";
            this.anti.ReadOnly = true;
            this.anti.Size = new System.Drawing.Size(146, 26);
            this.anti.TabIndex = 2;
            this.anti.Text = "Anti-Method?";
            // 
            // frequency
            // 
            this.frequency.Location = new System.Drawing.Point(176, 240);
            this.frequency.Name = "frequency";
            this.frequency.Size = new System.Drawing.Size(156, 26);
            this.frequency.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 48);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 20);
            this.label26.TabIndex = 7;
            this.label26.Text = "Spot Price : ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(27, 80);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 20);
            this.label25.TabIndex = 8;
            this.label25.Text = "Strike Price : ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(27, 112);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 20);
            this.label24.TabIndex = 9;
            this.label24.Text = "Tenor : ";
            // 
            // volatility
            // 
            this.volatility.Location = new System.Drawing.Point(176, 176);
            this.volatility.Name = "volatility";
            this.volatility.Size = new System.Drawing.Size(156, 26);
            this.volatility.TabIndex = 4;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(27, 144);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(115, 20);
            this.label23.TabIndex = 10;
            this.label23.Text = "Interest Rate : ";
            // 
            // tenor
            // 
            this.tenor.Location = new System.Drawing.Point(176, 112);
            this.tenor.Name = "tenor";
            this.tenor.Size = new System.Drawing.Size(156, 26);
            this.tenor.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(27, 176);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 20);
            this.label22.TabIndex = 11;
            this.label22.Text = "Volatility : ";
            // 
            // strikrPrice
            // 
            this.strikrPrice.Location = new System.Drawing.Point(176, 80);
            this.strikrPrice.Name = "strikrPrice";
            this.strikrPrice.Size = new System.Drawing.Size(156, 26);
            this.strikrPrice.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(29, 208);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 20);
            this.label21.TabIndex = 12;
            this.label21.Text = "Trails : ";
            // 
            // spotPrice
            // 
            this.spotPrice.Location = new System.Drawing.Point(176, 48);
            this.spotPrice.Name = "spotPrice";
            this.spotPrice.Size = new System.Drawing.Size(156, 26);
            this.spotPrice.TabIndex = 0;
            // 
            // rebate_barrier_label
            // 
            this.rebate_barrier_label.AutoSize = true;
            this.rebate_barrier_label.Location = new System.Drawing.Point(27, 272);
            this.rebate_barrier_label.Name = "rebate_barrier_label";
            this.rebate_barrier_label.Size = new System.Drawing.Size(74, 20);
            this.rebate_barrier_label.TabIndex = 17;
            this.rebate_barrier_label.Text = "Rebate : ";
            // 
            // interestRate
            // 
            this.interestRate.Location = new System.Drawing.Point(176, 144);
            this.interestRate.Name = "interestRate";
            this.interestRate.Size = new System.Drawing.Size(156, 26);
            this.interestRate.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 240);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 20);
            this.label19.TabIndex = 15;
            this.label19.Text = "Frequency :";
            // 
            // trails
            // 
            this.trails.Location = new System.Drawing.Point(176, 208);
            this.trails.Name = "trails";
            this.trails.Size = new System.Drawing.Size(156, 26);
            this.trails.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(28, 255);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 15);
            this.label18.TabIndex = 16;
            this.label18.Text = "(times per year)";
            // 
            // rebate_barrier
            // 
            this.rebate_barrier.Location = new System.Drawing.Point(176, 272);
            this.rebate_barrier.Name = "rebate_barrier";
            this.rebate_barrier.Size = new System.Drawing.Size(156, 26);
            this.rebate_barrier.TabIndex = 18;
            // 
            // WarnTxt
            // 
            this.WarnTxt.AutoSize = true;
            this.WarnTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WarnTxt.ForeColor = System.Drawing.Color.Red;
            this.WarnTxt.Location = new System.Drawing.Point(23, 381);
            this.WarnTxt.Name = "WarnTxt";
            this.WarnTxt.Size = new System.Drawing.Size(377, 24);
            this.WarnTxt.TabIndex = 2;
            this.WarnTxt.Text = "Please Complete The Information Required!";
            // 
            // BarrierType
            // 
            this.BarrierType.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BarrierType.Items.Add("Down&Out");
            this.BarrierType.Items.Add("Down&In");
            this.BarrierType.Items.Add("Up&Out");
            this.BarrierType.Items.Add("Up&In");
            this.BarrierType.Location = new System.Drawing.Point(38, 103);
            this.BarrierType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BarrierType.Name = "BarrierType";
            this.BarrierType.ReadOnly = true;
            this.BarrierType.Size = new System.Drawing.Size(146, 26);
            this.BarrierType.TabIndex = 20;
            this.BarrierType.Text = "Barrier Type";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(603, 423);
            this.Controls.Add(this.OptionBox);
            this.Controls.Add(this.WarnTxt);
            this.Controls.Add(this.SimulatorStart);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.groupBox3);
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.OptionBox.ResumeLayout(false);
            this.OptionBox.PerformLayout();
            this.TypeBox3.ResumeLayout(false);
            this.TypeBox3.PerformLayout();
            this.TypeBox.ResumeLayout(false);
            this.TypeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button SimulatorStart;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem operationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startSimulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem europeanOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rangeOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asianOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barrierOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookBackOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem digitalOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userGuideToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label WarnTxt;
        private System.Windows.Forms.Label CommingSoon;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox OptionBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox frequency;
        private System.Windows.Forms.GroupBox TypeBox;
        private System.Windows.Forms.TextBox threadAmt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DomainUpDown multi;
        private System.Windows.Forms.DomainUpDown cv;
        private System.Windows.Forms.DomainUpDown anti;
        private System.Windows.Forms.RadioButton put;
        private System.Windows.Forms.RadioButton call;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox trails;
        private System.Windows.Forms.TextBox volatility;
        private System.Windows.Forms.TextBox interestRate;
        private System.Windows.Forms.TextBox tenor;
        private System.Windows.Forms.TextBox strikrPrice;
        private System.Windows.Forms.TextBox spotPrice;
        private System.Windows.Forms.TextBox rebate_barrier;
        private System.Windows.Forms.Label rebate_barrier_label;
        private System.Windows.Forms.GroupBox TypeBox3;
        private System.Windows.Forms.DomainUpDown BarrierType;
    }
}

