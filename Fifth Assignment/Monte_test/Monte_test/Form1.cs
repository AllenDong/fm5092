﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

//注意，environment里作为参数传入greekletter的queue以抽象类或interface声明，thread使用simulator定义时要有参，然后在simulator里使用泛型初始化，同时还需在simulator里通过泛型进行有参实例化，并在输出结果时通过泛型输出类的属性。

namespace Monte_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            WarnTxt.Hide();
            groupBox3.Hide();
            BarrierType.Hide();
            OptionBox.Hide();
            SimulatorStart.Hide();
            threadAmt.Text = Convert.ToString(System.Environment.ProcessorCount);
            threadAmt.ReadOnly = true;
        }

        static public ResultForm resul = new ResultForm();

        private void button1_Click(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            try
            {
                resul.progressBar.Value = 0;
                if (anti.Text == "Yes") { Environment.anti_idx = 1; } else { Environment.anti_idx = 0; }
                if (cv.Text == "Yes") { Environment.cv_idx = 1; } else { Environment.cv_idx = 0; }
                Environment.spotPrice = Convert.ToDouble(spotPrice.Text);
                Environment.risklessInterest = Convert.ToDouble(interestRate.Text);
                Environment.volatility = Convert.ToDouble(volatility.Text);
                Environment.tenor = Convert.ToDouble(tenor.Text);
                Environment.trials = Convert.ToInt32(trails.Text);
                Environment.trading_frequency = Convert.ToInt32(frequency.Text);
                Environment.thread_amt = multi.Text == "Yes" ? Convert.ToInt32(threadAmt.Text) : 1;
                Environment.split = Environment.thread_amt;
                resul.progressBar.Maximum = Environment.trials * (Environment.anti_idx + 1) + 10 * Environment.split;
                if (call.Checked) { Environment.pc_idx = 1; } else { Environment.pc_idx = -1; }
                WarnTxt.Hide();
                switch (OptionBox.Text)
                {
                    case "European Option":
                        {
                            Euro_Init();
                            break;
                        }
                    case "Range Option":
                        {
                            Rang_Init();
                            break;
                        }
                    case "Asian Option":
                        {
                            Asia_Init();
                            break;
                        }
                    case "Digital Option":
                        {
                            Digi_Init();
                            break;
                        }
                    case "Lookback Option":
                        {
                            Look_Init();
                            break;
                        }
                    case "Barrier Option":
                        {
                            Barr_Init();
                            break;
                        }
                }
                resul.ShowDialog();
            }
            catch
            {
                WarnTxt.Show();
            }
        }

        private void europeanOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Hide();
            BarrierType.Hide();
            rebate_barrier_label.Hide();
            Environment.optionType = typeof(EuropeanOption);
            OptionBox.Text = "European Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void rangeOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Hide();
            BarrierType.Hide();
            rebate_barrier_label.Hide();
            Environment.optionType = typeof(RangeOption);
            OptionBox.Text = "Range Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void asianOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Hide();
            BarrierType.Hide();
            rebate_barrier_label.Hide();
            Environment.optionType = typeof(AsianOption);
            OptionBox.Text = "Asian Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void barrierOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Show();
            BarrierType.Show();
            rebate_barrier_label.Text = "Barrier :";
            rebate_barrier_label.Show();
            Environment.optionType = typeof(BarrierOption);
            OptionBox.Text = "Barrier Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void lookBackOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Hide();
            BarrierType.Hide();
            rebate_barrier_label.Hide();
            Environment.optionType = typeof(LookbackOption);
            OptionBox.Text = "Lookback Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void digitalOptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rebate_barrier.Show();
            BarrierType.Hide();
            rebate_barrier_label.Text = "Rebate :";
            rebate_barrier_label.Show();
            Environment.optionType = typeof(DigitalOption);
            OptionBox.Text = "Digital Option";
            OptionBox.Show();
            groupBox3.Hide();
            SimulatorStart.Show();
        }

        private void Euro_Init()
        {
            Environment.strikePrice = Convert.ToDouble(strikrPrice.Text);
        }

        private void Rang_Init()
        {
        }

        private void Asia_Init()
        {
            Environment.strikePrice = Convert.ToDouble(strikrPrice.Text);
        }

        private void Barr_Init()
        {
            Environment.barrier = Convert.ToDouble(rebate_barrier.Text);
            Environment.strikePrice = Convert.ToDouble(strikrPrice.Text);
            Environment.out_in_idx = (BarrierType.Text == "Up&In" || BarrierType.Text == "Down&In") ? 1 : -1;
            Environment.up_down_idx = (BarrierType.Text == "Up&In" || BarrierType.Text == "Up&Out") ? 1 : -1;
        }

        private void Digi_Init()
        {
            Environment.strikePrice = Convert.ToDouble(strikrPrice.Text);
            Environment.rebate = Convert.ToDouble(rebate_barrier.Text);
        }

        private void Look_Init()
        {
            Environment.strikePrice = Convert.ToDouble(strikrPrice.Text);
        }

        private void multi_SelectedItemChanged(object sender, EventArgs e)
        {
            threadAmt.ReadOnly = multi.Text == "Yes" ? false : true;
        }
    }
}
