﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;

namespace Monte_test
{
    class Environment
    {
        static public Type optionType;

        static public double spotPrice;
        static public double strikePrice;
        static public int step;
        static public double risklessInterest;
        static public double volatility;
        static public int trials;
        static public double pc_idx;
        static public int anti_idx;
        static public int cv_idx;
        static public int trading_frequency;
        static public int thread_amt;
        static public double rebate;
        static public int up_down_idx;
        static public int out_in_idx;
        static public double barrier;

        static public double unitInterval;
        static public double tenor;
        static public double discount;
        static public int split;
        static public int Thread_split = 2;

        static public double increS = 0.1; //0.001 for range; 0.1 for digital(gamma works badly); 
        static public double increR = 0.01; //0.01 for digital;
        static public double increV = 0.1; //0.01 for digital;

        static public List<List<ConcurrentQueue<double>>> rc_collection;
        static public ConcurrentQueue<IOptions> option_queue;
        static public Queue<double> temp;
        static public List<Queue<double>> price;
        static public List<Queue<double>> delta;
        static public List<Queue<double>> theta;
        static public List<Queue<double>> rho;
        static public List<Queue<double>> vega;
        static public List<Queue<double>> gamma;
        static public List<ConcurrentQueue<double>> cvList;
        static public List<ConcurrentQueue<double>> SN_next_List;
        static public List<ConcurrentQueue<double>> SN_List;
        static public double output_1 = 0;
        static public double output_2 = 0;
        static public int iter_record = 0;

        static public double out_price;
        static public double out_theta;
        static public double out_delta;
        static public double out_gamma;
        static public double out_rho;
        static public double out_vega;


        static public void init_environment()
        {
            step = Convert.ToInt32(Math.Ceiling(tenor * trading_frequency));
            discount = Math.Exp(-Environment.tenor * Environment.risklessInterest);
            unitInterval = 1.0 / trading_frequency;

            temp = new Queue<double>();
            price = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            delta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            theta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            rho = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            vega = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            gamma = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            cvList = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            SN_next_List = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            SN_List = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            rc_collection = new List<List<ConcurrentQueue<double>>>();
            Environment.option_queue = new ConcurrentQueue<IOptions>();
            iter_record = 0;
        }
        static public List<ConcurrentQueue<double>> fillQueue()
        {
            normalrandom randlist = new normalrandom(Convert.ToInt32(Math.Ceiling(1.0 * trials * (step + 1) / split)));
            List<ConcurrentQueue<double>> rc = new List<ConcurrentQueue<double>>(2) { randlist.randomlist_posi, randlist.randomlist_nega };
            rc_collection.Add(rc);
            return rc;
        }
        static public double se(Queue<double> queue)
        {
            double se = 0;
            double mean = queue.Average();
            double length = queue.Count();
            while (queue.Count() > 0)
            {
                double temp = queue.Dequeue();
                se = se + (temp - mean) * (temp - mean);
            }
            return (Math.Sqrt(se / (length * (length - 1))) * discount);
        }
        static public void pro_bar()
        {
            while (Form1.resul.progressBar.Value < Form1.resul.progressBar.Maximum)
            {
                if (Form1.resul.progressBar.Value != Environment.iter_record)
                {
                    try
                    {
                        Form1.resul.progressBar.Value = iter_record;
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
        public static void simulate()
        {
            Type optionType = Environment.optionType;
            ParallelOptions opt = new ParallelOptions();
            opt.MaxDegreeOfParallelism = Environment.thread_amt;
            Thread prog_bar = new Thread(new ThreadStart(Environment.pro_bar));
            prog_bar.Start();
            Parallel.For(0, Environment.split, opt, idx2 =>
            {
                Environment.fillQueue();
                Environment.iter_record = Environment.iter_record + 10;
            });
            Parallel.For(0, Environment.thread_amt, opt, idx2 =>
            {
                while (Environment.rc_collection.Count() < Environment.split) { Environment.rc_collection.Add(Environment.fillQueue()); }
                if (Environment.rc_collection[idx2] == null) { Environment.rc_collection[idx2] = Environment.fillQueue(); }
                while (Environment.rc_collection[idx2].Count() == 0) { }
                for (int idx = 0; idx <= Environment.anti_idx; idx++)
                {
                    while (Environment.rc_collection[idx2][0].Count() >= Environment.step)
                    {
                        var newOption = Activator.CreateInstance(optionType, Environment.rc_collection[idx2][0], idx);
                        Environment.option_queue.Enqueue((IOptions) newOption);
                        Environment.iter_record++;
                    }
                    Environment.rc_collection[idx2].RemoveAt(0);
                }
            });
            optionType.InvokeMember("greekLetter",BindingFlags.InvokeMethod|BindingFlags.Static|BindingFlags.Public,null,null,new object[] { Environment.option_queue });

            if (Environment.anti_idx == 1)
            {
                while (Environment.price[1].Count() > 0)
                {
                    Environment.price[0].Enqueue((Environment.price[0].Dequeue() + Environment.price[1].Dequeue()) / 2.0);
                    Environment.theta[0].Enqueue((Environment.theta[0].Dequeue() + Environment.theta[1].Dequeue()) / 2.0);
                    Environment.delta[0].Enqueue((Environment.delta[0].Dequeue() + Environment.delta[1].Dequeue()) / 2.0);
                    Environment.rho[0].Enqueue((Environment.rho[0].Dequeue() + Environment.rho[1].Dequeue()) / 2.0);
                    Environment.vega[0].Enqueue((Environment.vega[0].Dequeue() + Environment.vega[1].Dequeue()) / 2.0);
                    Environment.gamma[0].Enqueue((Environment.gamma[0].Dequeue() + Environment.gamma[1].Dequeue()) / 2.0);
                    if (Environment.cv_idx == 1)
                    {
                        Environment.cvList[0].TryDequeue(out Environment.output_1);
                        Environment.cvList[1].TryDequeue(out Environment.output_2);
                        Environment.cvList[0].Enqueue((Environment.output_1 + Environment.output_2) / 2.0);
                    }
                }
            }

            #region calculate the required results
            out_price = Environment.discount * Environment.price[0].Average();
            out_theta = -(Math.Exp(-(Environment.tenor + Environment.unitInterval) * Environment.risklessInterest) * Environment.theta[0].Average() - out_price) * Environment.step;
            out_delta = (Environment.discount * Environment.delta[0].Average() - out_price) / Environment.increS;
            out_rho = (Math.Exp(-Environment.increR * Environment.tenor) * Environment.discount * Environment.rho[0].Average() - out_price) / Environment.increR;
            out_vega = (Environment.discount * Environment.vega[0].Average() - out_price) / Environment.increV;
            out_gamma = Environment.discount * Environment.gamma[0].Average();

            if (Environment.cv_idx == 1)
            {
                DeltaCV.cvSn(Environment.price[0], Environment.cvList[0]);
                out_price = Environment.discount * Environment.price[0].Average();
            }
            Environment.iter_record = Form1.resul.progressBar.Maximum;
            #endregion
        }
    }
}
