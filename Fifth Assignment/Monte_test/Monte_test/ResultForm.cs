﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace Monte_test
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
        }
        private void ResultForm_Load(object sender, EventArgs e)
        {

        }

        static Thread simulator_thread;
        private void StartCal_Click(object sender, EventArgs e)
        {
            Form1.resul.progressBar.Value = 0;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            Environment.init_environment();
            try
            {
                simulator_thread = new Thread(new ThreadStart(Environment.simulate));//EuropeanOption.simulate));
                simulator_thread.Start();
                simulator_thread.Join();
                
            }
            catch
            {
                simulator_thread.Abort();
                simulator_thread = new Thread(new ThreadStart(Environment.simulate));//EuropeanOption.simulate));
                simulator_thread.Start();
                simulator_thread.Join();
            }
            StartCal.Text = "Price : " + Convert.ToString(Environment.out_price);
            thetabox.Text = Convert.ToString(Environment.out_theta);
            deltabox.Text = Convert.ToString(Environment.out_delta);
            gammabox.Text = Convert.ToString(Environment.out_gamma);
            rhobox.Text = Convert.ToString(Environment.out_rho);
            vegabox.Text = Convert.ToString(Environment.out_vega);
            stderror.Text = Convert.ToString(Environment.se(Environment.price[0]));
            watch.Stop();
            TimeSpan ts = new TimeSpan();
            ts = watch.Elapsed;
            TimeLen.Text = Convert.ToString(ts);
        }
    }
}
