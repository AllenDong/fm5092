﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class DeltaCV
    {
        private static double cdf(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            int sign;
            if (z < 0)
            {
                sign = -1;
            }
            else
            {
                sign = 1;
            }
            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double elf = 1.0 - ((((a5 * t + a4) * t + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);
            return 0.5 * (1.0 + sign * elf);
        }
        private static double d1(double S, double K, double r, double sigma, double T, double t)
        {
            double result = 0;
            result = (Math.Log(S / K) + (r + sigma * sigma / 2.0) * (T - t)) / (sigma * Math.Sqrt(T - t));
            return result;
        }
        public static double deltaGene(bool call, double S, double K, double r, double sigma, double T, double t)
        {
            double result = 0;
            result = cdf(d1(S, K, r, sigma, T, t));
            if (call == false)
            {
                result = result - 1;
            }
            return result;
        }
        public static void cvSn(Queue<double> SnList, ConcurrentQueue<double> cvList)
        {
            double N = SnList.Count();
            double sxy = 0;
            double sxx = 0;
            double beta;
            double priceMean = SnList.Average();
            double cvMean = cvList.Average();
            double[] priceArr = SnList.ToArray();
            double[] cvArr = cvList.ToArray();
            double temp;
            for (int idx = 0; idx < N; idx++)
            {
                sxy = sxy + ((priceArr[idx] - priceMean) * (cvArr[idx] - cvMean));
                sxx = sxx + ((cvArr[idx] - cvMean) * (cvArr[idx] - cvMean));
            }
            beta = sxy / sxx;
            while (cvList.Count() > 0)
            {
                double xx = 0;
                cvList.TryDequeue(out xx);
                temp = SnList.Dequeue() - beta * xx;
                SnList.Enqueue(temp);
            }
        }
    }
}
