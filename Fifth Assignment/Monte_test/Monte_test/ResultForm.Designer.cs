﻿namespace Monte_test
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartCal = new System.Windows.Forms.Button();
            this.thetabox = new System.Windows.Forms.TextBox();
            this.deltabox = new System.Windows.Forms.TextBox();
            this.gammabox = new System.Windows.Forms.TextBox();
            this.stderror = new System.Windows.Forms.TextBox();
            this.rhobox = new System.Windows.Forms.TextBox();
            this.vegabox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TimeLen = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartCal
            // 
            this.StartCal.Location = new System.Drawing.Point(161, 32);
            this.StartCal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.StartCal.Name = "StartCal";
            this.StartCal.Size = new System.Drawing.Size(198, 42);
            this.StartCal.TabIndex = 0;
            this.StartCal.Text = "Start";
            this.StartCal.UseVisualStyleBackColor = true;
            this.StartCal.Click += new System.EventHandler(this.StartCal_Click);
            // 
            // thetabox
            // 
            this.thetabox.Location = new System.Drawing.Point(59, 56);
            this.thetabox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.thetabox.Name = "thetabox";
            this.thetabox.Size = new System.Drawing.Size(136, 20);
            this.thetabox.TabIndex = 1;
            // 
            // deltabox
            // 
            this.deltabox.Location = new System.Drawing.Point(59, 14);
            this.deltabox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.deltabox.Name = "deltabox";
            this.deltabox.Size = new System.Drawing.Size(136, 20);
            this.deltabox.TabIndex = 2;
            // 
            // gammabox
            // 
            this.gammabox.Location = new System.Drawing.Point(59, 35);
            this.gammabox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gammabox.Name = "gammabox";
            this.gammabox.Size = new System.Drawing.Size(136, 20);
            this.gammabox.TabIndex = 3;
            // 
            // stderror
            // 
            this.stderror.Location = new System.Drawing.Point(59, 118);
            this.stderror.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stderror.Name = "stderror";
            this.stderror.Size = new System.Drawing.Size(136, 20);
            this.stderror.TabIndex = 4;
            // 
            // rhobox
            // 
            this.rhobox.Location = new System.Drawing.Point(59, 77);
            this.rhobox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rhobox.Name = "rhobox";
            this.rhobox.Size = new System.Drawing.Size(136, 20);
            this.rhobox.TabIndex = 5;
            // 
            // vegabox
            // 
            this.vegabox.Location = new System.Drawing.Point(59, 97);
            this.vegabox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.vegabox.Name = "vegabox";
            this.vegabox.Size = new System.Drawing.Size(136, 20);
            this.vegabox.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.progressBar);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.gammabox);
            this.groupBox1.Controls.Add(this.thetabox);
            this.groupBox1.Controls.Add(this.stderror);
            this.groupBox1.Controls.Add(this.vegabox);
            this.groupBox1.Controls.Add(this.rhobox);
            this.groupBox1.Controls.Add(this.deltabox);
            this.groupBox1.Location = new System.Drawing.Point(161, 71);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(198, 149);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(0, 139);
            this.progressBar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(198, 10);
            this.progressBar.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 118);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "STD E : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Vega : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 77);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Rho : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Theta : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Gamma : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Delta : ";
            // 
            // TimeLen
            // 
            this.TimeLen.AccessibleName = "TimeLen";
            this.TimeLen.Location = new System.Drawing.Point(364, 210);
            this.TimeLen.Name = "TimeLen";
            this.TimeLen.Size = new System.Drawing.Size(100, 20);
            this.TimeLen.TabIndex = 8;
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 232);
            this.Controls.Add(this.TimeLen);
            this.Controls.Add(this.StartCal);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ResultForm";
            this.Text = "ResultForm";
            this.Load += new System.EventHandler(this.ResultForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartCal;
        private System.Windows.Forms.TextBox thetabox;
        private System.Windows.Forms.TextBox deltabox;
        private System.Windows.Forms.TextBox gammabox;
        private System.Windows.Forms.TextBox stderror;
        private System.Windows.Forms.TextBox rhobox;
        private System.Windows.Forms.TextBox vegabox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox TimeLen;
    }
}