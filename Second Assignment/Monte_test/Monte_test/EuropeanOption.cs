﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class EuropeanOption : IOptions
    {
        public double finalValue;
        public double finalValue_theta;
        public double finalValue_delta;
        public double finalValue_delta2;
        public double finalValue_gamma;
        public double finalValue_rho;
        public double finalValue_vega;
        public Queue<double> priceList = new Queue<double>();

        public EuropeanOption(Queue<double> randlist)
        {
            pathGene(randlist);
        }

        public void pathGene(Queue<double> randlist)
        {
            Queue<double> temp = new Queue<double>();
            double lastPrice = Environment.spotPrice;
            double lastPrice_delta = 0;
            double lastPrice_delta2 = 0;
            double lastPrice_vega = 0;
            for (int idx = 0; idx < Environment.step+1; idx++)
            {
                temp.Enqueue(lastPrice);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * randlist.Dequeue());
            }

            temp.Enqueue(lastPrice);
            lastPrice_delta = temp.ElementAt(temp.Count() - 2) * (1 + Environment.increS / Environment.spotPrice);
            lastPrice_delta2 = temp.ElementAt(temp.Count() - 2) * (1 - Environment.increS / Environment.spotPrice);
            lastPrice_vega = temp.ElementAt(temp.Count() - 2) * Math.Exp(-(Environment.increV * Environment.increV / 2 + Environment.increV * Environment.volatility) * Environment.tenor + (Math.Log(temp.ElementAt(temp.Count() - 2) / Environment.spotPrice) - (Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.tenor) / Environment.volatility * Environment.increV);
            this.finalValue = Math.Max(0, Environment.pc_idx * (temp.ElementAt(temp.Count() - 2) - Environment.strikePrice));
            this.finalValue_delta = Math.Max(0, Environment.pc_idx * (lastPrice_delta - Environment.strikePrice));
            this.finalValue_delta2 = Math.Max(0, Environment.pc_idx * (lastPrice_delta2 - Environment.strikePrice));
            this.finalValue_gamma = (this.finalValue_delta - 2 * this.finalValue + this.finalValue_delta2) / (Environment.increS * Environment.increS);
            this.finalValue_theta = Math.Max(0, Environment.pc_idx * (temp.ElementAt(temp.Count()-1) - Environment.strikePrice));
            this.finalValue_rho = Math.Max(0, Environment.pc_idx * (Math.Exp(Environment.increR * Environment.tenor) * temp.ElementAt(temp.Count() - 2) - Environment.strikePrice));
            this.finalValue_vega = Math.Max(0, Environment.pc_idx * (lastPrice_vega - Environment.strikePrice));
        }

        public static double price = 0;
        public static double gamma = 0;
        public static double delta = 0;
        public static double theta = 0;
        public static double rho = 0;
        public static double vega = 0;

        public static void simulate()
        {
            double mean = 0;
            double mean_theta = 0;
            double mean_delta = 0;
            double mean_delta2 = 0;
            double mean_rho = 0;
            double mean_vega = 0;
            for (int idx2 = 0; idx2 < Environment.split; idx2++)
            {
                Environment.fillQueue();
                for (int i = 0; i <= Environment.anti_idx; i++)
                {
                    while (Environment.randlist_collection[0].Count() >= Environment.step)
                    {
                        EuropeanOption europeanOption = new EuropeanOption(Environment.randlist_collection[0]);
                        mean = mean + europeanOption.finalValue;
                        mean_theta = mean_theta + europeanOption.finalValue_theta;
                        mean_delta = mean_delta + europeanOption.finalValue_delta;
                        mean_rho = mean_rho + europeanOption.finalValue_rho;
                        mean_vega = mean_vega + europeanOption.finalValue_vega;
                        gamma = gamma + europeanOption.finalValue_gamma;
                        Environment.price.Enqueue(europeanOption.finalValue); 
                    }
                    Environment.randlist_collection.RemoveAt(0);
                }
                Form1.resul.progressBar.Value = Form1.resul.progressBar.Value + 1;
            }
            double times = (1 + Environment.anti_idx);
            mean = Environment.discount * mean / (Environment.trials * times);
            mean_theta = Math.Exp(-(Environment.tenor + Environment.unitInterval) * Environment.risklessInterest) * mean_theta / (Environment.trials * times);
            mean_delta = Environment.discount * mean_delta / (Environment.trials * times);
            mean_delta2 = Environment.discount * mean_delta2 / (Environment.trials * times);
            mean_rho = Math.Exp(-Environment.increR * Environment.tenor) * Environment.discount * mean_rho / (Environment.trials * times);
            mean_vega = Environment.discount * mean_vega / (Environment.trials * times);
            price = mean;
            delta = (mean_delta - mean) / Environment.increS;
            gamma = Environment.discount * gamma / (Environment.trials * times);
            theta = -(mean_theta - mean) * Environment.step;
            rho = (mean_rho - mean) / Environment.increR;
            vega = (mean_vega - mean) / Environment.increV;
            if (Environment.anti_idx == 1)
            {
                double[] temp = Environment.price.ToArray();
                for (int idx2 = 0; idx2 < Environment.split; idx2++)
                {
                    int basis = 2 * idx2 * Environment.trials / Environment.split;
                    int basis2 = (2 * idx2 + 1) * Environment.trials / Environment.split;
                    for (int idx = 0; idx < Environment.trials / Environment.split; idx++)
                    {
                        Environment.price_anti.Enqueue((temp[basis + idx] + temp[idx + basis2]) / 2);
                    }
                }
            }
        }
    }
}
