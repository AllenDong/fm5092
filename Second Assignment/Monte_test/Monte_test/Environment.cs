﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class Environment
    {
        static public double spotPrice;
        static public double strikePrice;
        static public int step;
        static public double risklessInterest;
        static public double volatility;
        static public int trials;
        static public double pc_idx;
        static public int anti_idx;
        static public int trading_frequency;

        static public double unitInterval;
        static public double tenor;
        static public double discount;
        static public int split = 1000;

        static public double increS = 0.01;
        static public double increR = 0.0001;
        static public double increV = 0.0001;

        static public List<Queue<double>> randlist_collection;
        static public Queue<double> temp;
        static public Queue<double> price;
        static public Queue<double> price_anti;


        static public void init_environment()
        {

            step = Convert.ToInt32(Math.Ceiling(tenor * trading_frequency));
            discount = Math.Exp(-Environment.tenor * Environment.risklessInterest);
            unitInterval = 1.0 / trading_frequency;
            temp = new Queue<double>();
            price = new Queue<double>();
            price_anti = new Queue<double>();
        }
        static public void fillQueue()
        {
            normalrandom randlist = new normalrandom(Convert.ToInt32(Math.Ceiling(1.0 * trials * (step + 1) / split)));
            randlist_collection = new List<Queue<double>>(2) { randlist.randomlist_posi, randlist.randomlist_nega };
        }
        static public double se(Queue<double> queue)
        {
            double se = 0;
            //Queue<double> temp_queue = new Queue<double>();
            //for (int idx = 0; idx < Environment.split * (1 + Environment.anti_idx); idx += 2)
            //{
            //    for (int idx2 = 0; idx2 < Environment.trials / Environment.split; idx2++)
            //    {
            //        temp_queue.Enqueue((queue.ElementAt(idx * Environment.trials / Environment.split + idx2) + queue.ElementAt((idx + 1) * Environment.trials / Environment.split + idx2)) / 2);
            //    }
            //}
            double mean = queue.Average();
            double length = queue.Count();
            while (queue.Count() > 0)
            {
                double temp = queue.Dequeue();
                se = se + (temp - mean) * (temp - mean);
            }
            return (Math.Sqrt(se / (length * (length - 1))) * discount);
        }
    }
}
