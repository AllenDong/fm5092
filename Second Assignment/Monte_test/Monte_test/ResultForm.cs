﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monte_test
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {

        }

        private void StartCal_Click(object sender, EventArgs e)
        {
            Form1.resul.progressBar.Value = 0;
            Environment.init_environment();
            EuropeanOption.simulate();
            StartCal.Text = "Price : "+Convert.ToString(EuropeanOption.price);
            thetabox.Text = Convert.ToString(EuropeanOption.theta);
            deltabox.Text = Convert.ToString(EuropeanOption.delta);
            gammabox.Text = Convert.ToString(EuropeanOption.gamma);
            rhobox.Text = Convert.ToString(EuropeanOption.rho);
            vegabox.Text = Convert.ToString(EuropeanOption.vega);
            if (Environment.anti_idx == 1) { stderror.Text = Convert.ToString(Environment.se(Environment.price_anti)); } else { stderror.Text = Convert.ToString(Environment.se(Environment.price)); }
        }
    }
}
