﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    class Environment
    {
        static public string optionType;

        static public double spotPrice;
        static public double strikePrice;
        static public int step;
        static public double risklessInterest;
        static public double volatility;
        static public int trials;
        static public double pc_idx;
        static public int anti_idx;
        static public int cv_idx;
        static public int trading_frequency;
        static public int thread_amt;

        static public double unitInterval;
        static public double tenor;
        static public double discount;
        static public int split;
        static public int Thread_split = 2;

        static public double increS = 0.01;
        static public double increR = 0.0001;
        static public double increV = 0.0001;

        static public List<List<ConcurrentQueue<double>>> rc_collection;
        static public ConcurrentQueue<EuropeanOption> Eu_queue;
        static public Queue<double> temp;
        static public List<Queue<double>> price;
        static public List<Queue<double>> delta;
        static public List<Queue<double>> theta;
        static public List<Queue<double>> rho;
        static public List<Queue<double>> vega;
        static public List<Queue<double>> gamma;
        static public List<ConcurrentQueue<double>> cvList;
        static public List<ConcurrentQueue<double>> SN_next_List;
        static public List<ConcurrentQueue<double>> SN_List;
        static public double output_1 = 0;
        static public double output_2 = 0;
        static public int iter_record = 0;


        static public void init_environment()
        {
            step = Convert.ToInt32(Math.Ceiling(tenor * trading_frequency));
            discount = Math.Exp(-Environment.tenor * Environment.risklessInterest);
            unitInterval = 1.0 / trading_frequency;

            temp = new Queue<double>();
            price = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            delta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            theta = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            rho = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            vega = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            gamma = new List<Queue<double>>() { new Queue<double>(), new Queue<double>() };
            cvList = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            SN_next_List = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            SN_List = new List<ConcurrentQueue<double>>() { new ConcurrentQueue<double>(), new ConcurrentQueue<double>() };
            rc_collection = new List<List<ConcurrentQueue<double>>>();
            Eu_queue = new ConcurrentQueue<EuropeanOption>();
            iter_record = 0;
        }
        static public List<ConcurrentQueue<double>> fillQueue()
        {
            normalrandom randlist = new normalrandom(Convert.ToInt32(Math.Ceiling(1.0 * trials * (step + 1) / split)));
            List<ConcurrentQueue<double>> rc = new List<ConcurrentQueue<double>>(2) { randlist.randomlist_posi, randlist.randomlist_nega };
            rc_collection.Add(rc);
            return rc;
        }
        static public double se(Queue<double> queue)
        {
            double se = 0;
            double mean = queue.Average();
            double length = queue.Count();
            while (queue.Count() > 0)
            {
                double temp = queue.Dequeue();
                se = se + (temp - mean) * (temp - mean);
            }
            return (Math.Sqrt(se / (length * (length - 1))) * discount);
        }
        static public void pro_bar()
        {
            while (Form1.resul.progressBar.Value < Form1.resul.progressBar.Maximum)
            {
                if (Form1.resul.progressBar.Value != Environment.iter_record)
                {
                    try
                    {
                        Form1.resul.progressBar.Value = iter_record;
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
    }
}
