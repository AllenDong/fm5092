﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Monte_test
{
    class EuropeanOption : IOptions
    {
        public Queue<double> path = new Queue<double>();
        public double contro_var;
        public int pn_idx;
        public EuropeanOption(Queue<double> randlist,int pn_idx)
        {
            this.pn_idx = pn_idx;
            pathGene(randlist);
        }
        public void pathGene(Queue<double> randlist)
        {
            double delta;
            double advRate = Math.Exp(Environment.risklessInterest * Environment.unitInterval);
            double lastPrice = Environment.spotPrice;
            double temp;
            for (int idx = 0; idx < Environment.step + 1; idx++)
            {
                temp = lastPrice;
                path.Enqueue(lastPrice);
                lastPrice = lastPrice * Math.Exp(((Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.unitInterval) + Environment.volatility * Math.Sqrt(Environment.unitInterval) * randlist.Dequeue());
                if (Environment.cv_idx == 1)
                {
                    delta = DeltaCV.deltaGene(Environment.pc_idx==1? true : false, temp, Environment.strikePrice, Environment.risklessInterest, Environment.volatility, Environment.tenor, idx * Environment.unitInterval);
                    if (idx != Environment.step)
                    {
                        contro_var = contro_var + delta * (lastPrice - temp * advRate);
                    }
                }
            }
                Environment.cvList[pn_idx].Enqueue(contro_var);
                Environment.SN_List[pn_idx].Enqueue(path.ElementAt(path.Count() - 2));
                Environment.SN_next_List[pn_idx].Enqueue(path.ElementAt(path.Count() - 1));

        }

        public static double price = 0;
        public static double gamma = 0;
        public static double delta = 0;
        public static double theta = 0;
        public static double rho = 0;
        public static double vega = 0;
        public static void greekLetter(List<ConcurrentQueue<double>> SN_list, List<ConcurrentQueue<double>> SN_next_list)
        {
            for (int idx = 0; idx <= Environment.anti_idx; idx++)
            {
                while (SN_list[idx].Count() > 0)
                {
                    double SN = 0;
                    while (!SN_list[idx].TryDequeue(out SN)) { }
                    double SN_next = 0;
                    while (!SN_next_list[idx].TryDequeue(out SN_next)) { }
                    double finalValue = Math.Max(0, Environment.pc_idx * (SN - Environment.strikePrice));
                    double finalValue_delta = Math.Max(0, Environment.pc_idx * (SN * (1 + Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                    double finalValue_delta2 = Math.Max(0, Environment.pc_idx * (SN * (1 - Environment.increS / Environment.spotPrice) - Environment.strikePrice));
                    double finalValue_gamma = (finalValue_delta - 2 * finalValue + finalValue_delta2) / (Environment.increS * Environment.increS);
                    double finalValue_theta = Math.Max(0, Environment.pc_idx * (SN_next - Environment.strikePrice));
                    double finalValue_rho = Math.Max(0, Environment.pc_idx * (Math.Exp(Environment.increR * Environment.tenor) * SN - Environment.strikePrice));
                    double finalValue_vega = Math.Max(0, Environment.pc_idx * (SN * Math.Exp(-(Environment.increV * Environment.increV / 2 + Environment.increV * Environment.volatility) * Environment.tenor + (Math.Log(SN / Environment.spotPrice) - (Environment.risklessInterest - Environment.volatility * Environment.volatility / 2) * Environment.tenor) / Environment.volatility * Environment.increV) - Environment.strikePrice));
                    Environment.price[idx].Enqueue(finalValue);
                    Environment.theta[idx].Enqueue(finalValue_theta);
                    Environment.delta[idx].Enqueue(finalValue_delta);
                    Environment.rho[idx].Enqueue(finalValue_rho);
                    Environment.vega[idx].Enqueue(finalValue_vega);
                    Environment.gamma[idx].Enqueue(finalValue_gamma);
                }
            }
        }
        public static void simulate()
        {
            ParallelOptions opt = new ParallelOptions();
            opt.MaxDegreeOfParallelism = Environment.thread_amt;

            //for (int iter = 0; iter < 4; iter++)
            //{
                for(int idx2=0;idx2<Environment.split;idx2++) //Parallel.For(0, Environment.split / 4, opt, idx2 =>
                {
                    Environment.fillQueue();
                //}
                //for(int idx2=0;idx2<Environment.split;idx2++) //Parallel.For(0, Environment.split / 4, opt, idx2 =>
                //{
                    //while (Environment.rc_collection.Count() < Environment.split / 4) { }
                    //while (Environment.rc_collection[idx2].Count() == 0 || Environment.rc_collection[idx2] == null) { }
                    for (int idx = 0; idx <= Environment.anti_idx; idx++)
                    {
                        while ((Environment.rc_collection[idx2])[0].Count() >= Environment.step)
                        {
                            EuropeanOption europeanOption = new EuropeanOption((Environment.rc_collection[idx2])[0], idx);
                        }
                        Environment.rc_collection[idx2].RemoveAt(0);
                    }
                    //Form1.resul.progressBar.Value = Form1.resul.progressBar.Value + 1;
                }
                //Environment.rc_collection.Clear();
            //}
            greekLetter(Environment.SN_List, Environment.SN_next_List);

            if (Environment.anti_idx == 1)
            {
                while (Environment.price[1].Count() > 0)
                {
                    Environment.price[0].Enqueue((Environment.price[0].Dequeue() + Environment.price[1].Dequeue()) / 2.0);
                    Environment.theta[0].Enqueue((Environment.theta[0].Dequeue() + Environment.theta[1].Dequeue()) / 2.0);
                    Environment.delta[0].Enqueue((Environment.delta[0].Dequeue() + Environment.delta[1].Dequeue()) / 2.0);
                    Environment.rho[0].Enqueue((Environment.rho[0].Dequeue() + Environment.rho[1].Dequeue()) / 2.0);
                    Environment.vega[0].Enqueue((Environment.vega[0].Dequeue() + Environment.vega[1].Dequeue()) / 2.0);
                    Environment.gamma[0].Enqueue((Environment.gamma[0].Dequeue() + Environment.gamma[1].Dequeue()) / 2.0);
                    if (Environment.cv_idx == 1)
                    {
                        while (!Environment.cvList[0].TryDequeue(out Environment.output_1)) { }
                        while (!Environment.cvList[1].TryDequeue(out Environment.output_2)) { }
                        Environment.cvList[0].Enqueue((Environment.output_1 + Environment.output_2) / 2.0);
                    }
                }
            }

            #region calculate the required results
            price = Environment.discount * Environment.price[0].Average();
            theta = -(Math.Exp(-(Environment.tenor + Environment.unitInterval) * Environment.risklessInterest) * Environment.theta[0].Average() - price) * Environment.step;
            delta = (Environment.discount * Environment.delta[0].Average() - price) / Environment.increS;
            rho = (Math.Exp(-Environment.increR * Environment.tenor) * Environment.discount * Environment.rho[0].Average() - price) / Environment.increR;
            vega = (Environment.discount * Environment.vega[0].Average() - price) / Environment.increV;
            gamma = Environment.discount * Environment.gamma[0].Average();

            if (Environment.cv_idx == 1)
            {
                DeltaCV.cvSn(Environment.price[0], Environment.cvList[0]);
                price = Environment.discount * Environment.price[0].Average();
            }
            #endregion
        }
    }
}
