﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monte_test
{
    interface IConvertible
    {
        TypeCode GetTypeCode();
        bool ToBoolean(IFormatProvider provider);
        byte ToByte(IFormatProvider provider);
    }
}
