﻿namespace Monte_test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SimulatorStart = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.operationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startSimulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.europeanOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.americanOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asianOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barrierOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lookBackOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.digitalOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userGuideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.frequencybox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ControlvarUse = new System.Windows.Forms.DomainUpDown();
            this.AntitheticUse = new System.Windows.Forms.DomainUpDown();
            this.PutButton = new System.Windows.Forms.RadioButton();
            this.CallButton = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TrailInput = new System.Windows.Forms.TextBox();
            this.VolatilityInput = new System.Windows.Forms.TextBox();
            this.InterestRateInput = new System.Windows.Forms.TextBox();
            this.TenorInput = new System.Windows.Forms.TextBox();
            this.StrikePriceInput = new System.Windows.Forms.TextBox();
            this.SpotPriceInput = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CommingSoon = new System.Windows.Forms.Label();
            this.WarnTxt = new System.Windows.Forms.Label();
            this.Thread_amt = new System.Windows.Forms.DomainUpDown();
            this.menuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // SimulatorStart
            // 
            this.SimulatorStart.Location = new System.Drawing.Point(430, 346);
            this.SimulatorStart.Name = "SimulatorStart";
            this.SimulatorStart.Size = new System.Drawing.Size(147, 37);
            this.SimulatorStart.TabIndex = 4;
            this.SimulatorStart.Text = "Start Simulator";
            this.SimulatorStart.UseVisualStyleBackColor = true;
            this.SimulatorStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operationToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(636, 32);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "UseIt";
            // 
            // operationToolStripMenuItem
            // 
            this.operationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startSimulatorToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.operationToolStripMenuItem.Name = "operationToolStripMenuItem";
            this.operationToolStripMenuItem.Size = new System.Drawing.Size(110, 28);
            this.operationToolStripMenuItem.Text = "Operation";
            // 
            // startSimulatorToolStripMenuItem
            // 
            this.startSimulatorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.europeanOptionToolStripMenuItem,
            this.americanOptionToolStripMenuItem,
            this.asianOptionToolStripMenuItem,
            this.barrierOptionToolStripMenuItem,
            this.lookBackOptionToolStripMenuItem,
            this.digitalOptionToolStripMenuItem});
            this.startSimulatorToolStripMenuItem.Name = "startSimulatorToolStripMenuItem";
            this.startSimulatorToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.startSimulatorToolStripMenuItem.Text = "Start Simulator";
            // 
            // europeanOptionToolStripMenuItem
            // 
            this.europeanOptionToolStripMenuItem.Name = "europeanOptionToolStripMenuItem";
            this.europeanOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.europeanOptionToolStripMenuItem.Text = "European Option";
            this.europeanOptionToolStripMenuItem.Click += new System.EventHandler(this.europeanOptionToolStripMenuItem_Click);
            // 
            // americanOptionToolStripMenuItem
            // 
            this.americanOptionToolStripMenuItem.Name = "americanOptionToolStripMenuItem";
            this.americanOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.americanOptionToolStripMenuItem.Text = "American Option";
            this.americanOptionToolStripMenuItem.Click += new System.EventHandler(this.americanOptionToolStripMenuItem_Click);
            // 
            // asianOptionToolStripMenuItem
            // 
            this.asianOptionToolStripMenuItem.Name = "asianOptionToolStripMenuItem";
            this.asianOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.asianOptionToolStripMenuItem.Text = "Asian Option";
            this.asianOptionToolStripMenuItem.Click += new System.EventHandler(this.asianOptionToolStripMenuItem_Click);
            // 
            // barrierOptionToolStripMenuItem
            // 
            this.barrierOptionToolStripMenuItem.Name = "barrierOptionToolStripMenuItem";
            this.barrierOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.barrierOptionToolStripMenuItem.Text = "Barrier Option";
            this.barrierOptionToolStripMenuItem.Click += new System.EventHandler(this.barrierOptionToolStripMenuItem_Click);
            // 
            // lookBackOptionToolStripMenuItem
            // 
            this.lookBackOptionToolStripMenuItem.Name = "lookBackOptionToolStripMenuItem";
            this.lookBackOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.lookBackOptionToolStripMenuItem.Text = "LookBack Option";
            this.lookBackOptionToolStripMenuItem.Click += new System.EventHandler(this.lookBackOptionToolStripMenuItem_Click);
            // 
            // digitalOptionToolStripMenuItem
            // 
            this.digitalOptionToolStripMenuItem.Name = "digitalOptionToolStripMenuItem";
            this.digitalOptionToolStripMenuItem.Size = new System.Drawing.Size(240, 30);
            this.digitalOptionToolStripMenuItem.Text = "Digital Option";
            this.digitalOptionToolStripMenuItem.Click += new System.EventHandler(this.digitalOptionToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userGuideToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(63, 28);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // userGuideToolStripMenuItem
            // 
            this.userGuideToolStripMenuItem.Name = "userGuideToolStripMenuItem";
            this.userGuideToolStripMenuItem.Size = new System.Drawing.Size(186, 30);
            this.userGuideToolStripMenuItem.Text = "User Guide";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.frequencybox);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TrailInput);
            this.groupBox1.Controls.Add(this.VolatilityInput);
            this.groupBox1.Controls.Add(this.InterestRateInput);
            this.groupBox1.Controls.Add(this.TenorInput);
            this.groupBox1.Controls.Add(this.StrikePriceInput);
            this.groupBox1.Controls.Add(this.SpotPriceInput);
            this.groupBox1.Location = new System.Drawing.Point(27, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(550, 274);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "European Option";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(42, 254);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "(times per year)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 232);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "Frequency :";
            // 
            // frequencybox
            // 
            this.frequencybox.Location = new System.Drawing.Point(188, 232);
            this.frequencybox.Name = "frequencybox";
            this.frequencybox.Size = new System.Drawing.Size(156, 26);
            this.frequencybox.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Thread_amt);
            this.groupBox2.Controls.Add(this.ControlvarUse);
            this.groupBox2.Controls.Add(this.AntitheticUse);
            this.groupBox2.Controls.Add(this.PutButton);
            this.groupBox2.Controls.Add(this.CallButton);
            this.groupBox2.Location = new System.Drawing.Point(351, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 272);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Type";
            // 
            // ControlvarUse
            // 
            this.ControlvarUse.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ControlvarUse.Items.Add("Yes");
            this.ControlvarUse.Items.Add("No");
            this.ControlvarUse.Location = new System.Drawing.Point(38, 191);
            this.ControlvarUse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ControlvarUse.Name = "ControlvarUse";
            this.ControlvarUse.ReadOnly = true;
            this.ControlvarUse.Size = new System.Drawing.Size(146, 26);
            this.ControlvarUse.TabIndex = 3;
            this.ControlvarUse.Text = "CV-Method?";
            // 
            // AntitheticUse
            // 
            this.AntitheticUse.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AntitheticUse.Items.Add("Yes");
            this.AntitheticUse.Items.Add("No");
            this.AntitheticUse.Location = new System.Drawing.Point(38, 155);
            this.AntitheticUse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AntitheticUse.Name = "AntitheticUse";
            this.AntitheticUse.ReadOnly = true;
            this.AntitheticUse.Size = new System.Drawing.Size(146, 26);
            this.AntitheticUse.TabIndex = 2;
            this.AntitheticUse.Text = "Anti-Method?";
            this.AntitheticUse.SelectedItemChanged += new System.EventHandler(this.AntitheticUse_SelectedItemChanged);
            // 
            // PutButton
            // 
            this.PutButton.AutoSize = true;
            this.PutButton.Location = new System.Drawing.Point(38, 95);
            this.PutButton.Name = "PutButton";
            this.PutButton.Size = new System.Drawing.Size(109, 24);
            this.PutButton.TabIndex = 1;
            this.PutButton.TabStop = true;
            this.PutButton.Text = "Put Option";
            this.PutButton.UseVisualStyleBackColor = true;
            // 
            // CallButton
            // 
            this.CallButton.AutoSize = true;
            this.CallButton.Checked = true;
            this.CallButton.Location = new System.Drawing.Point(38, 46);
            this.CallButton.Name = "CallButton";
            this.CallButton.Size = new System.Drawing.Size(111, 24);
            this.CallButton.TabIndex = 0;
            this.CallButton.TabStop = true;
            this.CallButton.Text = "Call Option";
            this.CallButton.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Trails : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Volatility : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Interest Rate : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tenor : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Strike Price : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Spot Price : ";
            // 
            // TrailInput
            // 
            this.TrailInput.Location = new System.Drawing.Point(188, 200);
            this.TrailInput.Name = "TrailInput";
            this.TrailInput.Size = new System.Drawing.Size(156, 26);
            this.TrailInput.TabIndex = 5;
            // 
            // VolatilityInput
            // 
            this.VolatilityInput.Location = new System.Drawing.Point(188, 168);
            this.VolatilityInput.Name = "VolatilityInput";
            this.VolatilityInput.Size = new System.Drawing.Size(156, 26);
            this.VolatilityInput.TabIndex = 4;
            // 
            // InterestRateInput
            // 
            this.InterestRateInput.Location = new System.Drawing.Point(188, 137);
            this.InterestRateInput.Name = "InterestRateInput";
            this.InterestRateInput.Size = new System.Drawing.Size(156, 26);
            this.InterestRateInput.TabIndex = 3;
            // 
            // TenorInput
            // 
            this.TenorInput.Location = new System.Drawing.Point(188, 103);
            this.TenorInput.Name = "TenorInput";
            this.TenorInput.Size = new System.Drawing.Size(156, 26);
            this.TenorInput.TabIndex = 2;
            // 
            // StrikePriceInput
            // 
            this.StrikePriceInput.Location = new System.Drawing.Point(188, 72);
            this.StrikePriceInput.Name = "StrikePriceInput";
            this.StrikePriceInput.Size = new System.Drawing.Size(156, 26);
            this.StrikePriceInput.TabIndex = 1;
            // 
            // SpotPriceInput
            // 
            this.SpotPriceInput.Location = new System.Drawing.Point(188, 40);
            this.SpotPriceInput.Name = "SpotPriceInput";
            this.SpotPriceInput.Size = new System.Drawing.Size(156, 26);
            this.SpotPriceInput.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CommingSoon);
            this.groupBox3.Location = new System.Drawing.Point(26, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(552, 274);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Warning";
            // 
            // CommingSoon
            // 
            this.CommingSoon.AutoSize = true;
            this.CommingSoon.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CommingSoon.Location = new System.Drawing.Point(50, 118);
            this.CommingSoon.Name = "CommingSoon";
            this.CommingSoon.Size = new System.Drawing.Size(458, 69);
            this.CommingSoon.TabIndex = 7;
            this.CommingSoon.Text = "Comming Soon!";
            // 
            // WarnTxt
            // 
            this.WarnTxt.AutoSize = true;
            this.WarnTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WarnTxt.ForeColor = System.Drawing.Color.Red;
            this.WarnTxt.Location = new System.Drawing.Point(22, 346);
            this.WarnTxt.Name = "WarnTxt";
            this.WarnTxt.Size = new System.Drawing.Size(377, 24);
            this.WarnTxt.TabIndex = 2;
            this.WarnTxt.Text = "Please Complete The Information Required!";
            // 
            // Thread_amt
            // 
            this.Thread_amt.Font = new System.Drawing.Font("SimSun", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Thread_amt.Items.Add("1");
            this.Thread_amt.Items.Add("2");
            this.Thread_amt.Items.Add("3");
            this.Thread_amt.Items.Add("4");
            this.Thread_amt.Location = new System.Drawing.Point(38, 227);
            this.Thread_amt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Thread_amt.Name = "Thread_amt";
            this.Thread_amt.ReadOnly = true;
            this.Thread_amt.Size = new System.Drawing.Size(146, 26);
            this.Thread_amt.TabIndex = 4;
            this.Thread_amt.Text = "Thread Amount";
            this.Thread_amt.SelectedItemChanged += new System.EventHandler(this.Thread_amt_SelectedItemChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(636, 445);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.WarnTxt);
            this.Controls.Add(this.SimulatorStart);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.groupBox3);
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button SimulatorStart;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem operationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startSimulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem europeanOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem americanOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asianOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barrierOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookBackOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem digitalOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userGuideToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TrailInput;
        private System.Windows.Forms.TextBox VolatilityInput;
        private System.Windows.Forms.TextBox InterestRateInput;
        private System.Windows.Forms.TextBox TenorInput;
        private System.Windows.Forms.TextBox StrikePriceInput;
        private System.Windows.Forms.TextBox SpotPriceInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton PutButton;
        private System.Windows.Forms.RadioButton CallButton;
        private System.Windows.Forms.Label WarnTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox frequencybox;
        private System.Windows.Forms.Label CommingSoon;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DomainUpDown ControlvarUse;
        private System.Windows.Forms.DomainUpDown AntitheticUse;
        private System.Windows.Forms.DomainUpDown Thread_amt;
    }
}

